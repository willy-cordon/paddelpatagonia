<?php

use Faker\Generator as Faker;

$factory->define(App\Trip::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'price' => $faker->randomFloat(2, 800, 3000),
        'body' => $faker->paragraph(3),
        'itinerary' => $faker->paragraph,
        'accomodations' => $faker->paragraph,
        'logistic' => $faker->paragraph,
    ];
});
