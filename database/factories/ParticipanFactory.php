<?php

use Faker\Generator as Faker;

$factory->define(App\Participant::class, function (Faker $faker) {
    return [
        'event_id' => \App\Event::inRandomOrder()->get()->pluck('id')->first(),
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'notes' => $faker->sentence,
        'paid' => rand(0,1)
    ];
});
