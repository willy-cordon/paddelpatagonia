<?php

use Faker\Generator as Faker;

$factory->define(App\Event::class, function (Faker $faker) {
    return [
        'trip_id' => \App\Trip::inRandomOrder()->get()->pluck('id')->first(),
        'price' => $faker->randomFloat(2, 800, 3000),
        'date' => $faker->dateTimeBetween($startDate = '-6 months', $endDate = '6 months'),
        'duration' => rand(1, 4),
        'availability' => rand(0, 12),
        'starting_point' => $faker->sentence,
        'summary' => $faker->paragraph
    ];
});
