<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdminThemeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');

        // Admin
        $admin = Role::create(['name' => 'admin']);
        $admin->givePermissionTo([
            Permission::create(['name' => 'users-create']),
            Permission::create(['name' => 'users-update']),
            Permission::create(['name' => 'users-delete']),
        ]);

        $manager = Role::create(['name' => 'manager']);
        $manager->givePermissionTo([
            Permission::create(['name' => 'manage-trips']),
            Permission::create(['name' => 'manage-events']),
        ]);

        /**
         * Here you can create all the roles and permissinon needed for you application
         * Just folow the example with the $admin role above.
         */

        $user = User::create([
            'name' => 'Emiliano',
            'email' => 'info@thormaweb.com',
            'password' => bcrypt('pass')
        ]);

        $user->assignRole(['admin', 'manager']);

    }
}
