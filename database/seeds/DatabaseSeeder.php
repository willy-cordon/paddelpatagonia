<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Call the Admin Theme seeder.
        $this->call(AdminThemeSeeder::class);

        if (\App::isLocal()) {
            factory(\App\Trip::class, 5)->create();
            factory(\App\Event::class, 20)->create();
            factory(\App\Participant::class, 100)->create();
        }
    }
}