<?php

return [

    'submit' => 'Submit',
    'actions' => 'Actions',
    'save' => 'Save',
    'cancel' => 'Cancel',
    'yes' => 'Yes',
	'no' => 'No',

	'support' => 'Support',
];
