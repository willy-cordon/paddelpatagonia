<?php

return [

    'submit' => 'Enviar',
    'actions' => 'Acciones',
    'save' => 'Guardar',
    'cancel' => 'Cancelar',
    'yes' => 'Si',
    'no' => 'No',
	
	'support' => 'Soporte',
];
