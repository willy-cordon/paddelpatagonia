@extends(config('admin-theme.master_layout'))

@section('content')

    @adminTheme('card', ['title' => 'Create Event'])

        @adminTheme('form', ['files' => true])

            <div class="row" style="margin:0px">
                <div class="col-md-6 col-sm-12">
                    @adminThemeSelect([
                        'name' => 'trip_id',
                        'label' => 'Trip',
                        'options' => \iVirtual\AdminTheme\AdminTheme::generateSelectOptions($trips, 'id', 'title')
                    ])
                    @push('scripts')
                    <script type="text/javascript">
                        var trips = '{!! $trips->toJson() !!}';

                        function findTripByKey(array, key, value) {
                            for (var i = 0; i < array.length; i++) {
                                if (array[i][key] === value) {
                                    return array[i];
                                }
                            }
                            return null;
                        }

                        $("select[name='trip_id']").on('change', function () {
                            $('input[name="price"]').val(findTripByKey(JSON.parse(trips), 'id', parseInt(this.value)).price).change()
                        });
                    </script>
                    @endpush
                </div>
                <div class="col-md-3 col-sm-12">
                    @adminThemeInput(['name' => 'price', 'label' => 'Price', 'type' => 'number'])
                </div>
                <div class="col-md-3 col-sm-12">
                    @adminThemeDate(['name' => 'date', 'label' => 'Date', 'format' => 'Y-m-d'])
                </div>
                <div class="col-md-3 col-sm-12">
                    @adminThemeInput(['name' => 'duration', 'label' => 'Duration in days', 'type' => 'number'])
                </div>
                <div class="col-md-3 col-sm-12">
                    @adminThemeInput(['name' => 'availability', 'label' => 'Total availability', 'type' => 'number'])
                </div>
                <div class="col-md-6 col-sm-12">
                    @adminThemeInput(['name' => 'starting_point', 'label' => 'Starting point'])
                </div>
            </div>

            @adminThemeTextarea(['name' => 'summary', 'label' => 'Summary', 'editor' => true])

        @endAdminTheme

    @endAdminTheme

@endsection