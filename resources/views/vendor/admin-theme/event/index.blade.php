@extends(config('admin-theme.master_layout'))

@section('content')

    @if(session()->has('status'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    @endif
    
    @adminTheme('card', ['title' => 'View Events'])


        @slot('actions')

            @adminThemeButton(['text' => 'Create Event', 'url' => url()->route('events.create')])

        @endslot


        @adminTheme('table', ['actions' => '25'])

            @slot('head')

                <th>Trip</th>

                <th>Date</th>

                <th>Price</th>

            @endslot

            @foreach($events as $event)

                <tr>

                    <td>{{ $event->trip_title }}</td>

                    <td>{{ $event->formated_date }}</td>

                    <td>{{ $event->formated_price }}</td>

                    <td class="text-center">

                        @adminThemeButton([
                            'url' => url()->route('participants.byEvent', ['slug' => $event->id]),
                            'icon' => 'group',
                            'style' => 'flat',
                            'size' => 'sm',
                            'hover' => 'Participants'
                        ])

                        @adminThemeButton([
                            'url' => url()->route('singleEvent', ['slug' => $event->slug]),
                            'target' => '_blank',
                            'icon' => 'remove_red_eye',
                            'style' => 'flat',
                            'size' => 'sm',
                            'hover' => 'View'
                        ])


                        @adminThemeButton([
                            'url' => url()->route('events.edit', ['id' => $event->id]),
                            'icon' => 'edit',
                            'style' => 'flat',
                            'size' => 'sm',
                            'hover' => 'Update'
                        ])


                        @adminThemeDeletePopup([
                            'id' => $event->id,
                            'url' => url()->route('events.delete', ['id' => $event->id]),
                        ])

                    </td>

                </tr>

            @endforeach

        @endAdminTheme

        <div style="float: right">{{ $events->links() }}</div>

    @endAdminTheme

@endsection