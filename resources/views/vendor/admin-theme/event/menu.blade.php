
@can('manage-events')

    @adminTheme('menu_item', ['icon' => 'date_range'])

        Events

        @slot('children')

            @adminTheme('menu_item', ['url' => url()->route('events.index')])

            View evnts

            @endAdminTheme


            @adminTheme('menu_item', ['url' => url()->route('events.create')])

            Create event

            @endAdminTheme


        @endslot

    @endAdminTheme

@endcan