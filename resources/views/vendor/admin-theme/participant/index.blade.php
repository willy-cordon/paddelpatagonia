@extends(config('admin-theme.master_layout'))

@section('content')

    @if(session()->has('status'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    @endif
    
    @adminTheme('card', ['title' => 'View Participants'])


        @slot('actions')

            {{-- @adminThemeButton(['text' => 'Create Event', 'url' => url()->route('participants.create')]) --}}

        @endslot


        @adminTheme('table', ['actions' => '25'])

            @slot('head')

                <th>Name</th>

                <th>Phone</th>

                <th>Emial</th>

                <th>Paid?</th>

            @endslot

            @foreach($participants as $participant)

                <tr>

                    <td>{{ $participant->name }}</td>

                    <td>{{ $participant->phone }}</td>

                    <td>{{ $participant->email }}</td>

                    <td>{{ $participant->paid ? 'Yes' : 'No' }}</td>

                    <td class="text-center">

                        {{-- @adminThemeButton([
                            'url' => url()->route('singleEvent', ['slug' => $event->slug]),
                            'target' => '_blank',
                            'icon' => 'remove_red_eye',
                            'style' => 'flat',
                            'size' => 'sm',
                            'hover' => 'View'
                        ]) --}}


                        {{-- @adminThemeButton([
                            'url' => url()->route('participants.edit', ['id' => $event->id]),
                            'icon' => 'edit',
                            'style' => 'flat',
                            'size' => 'sm',
                            'hover' => 'Update'
                        ])


                        @adminThemeDeletePopup([
                            'id' => $event->id,
                            'url' => url()->route('participants.delete', ['id' => $event->id]),
                        ]) --}}

                    </td>

                </tr>

            @endforeach

        @endAdminTheme

        {{-- <div style="float: right">{{ $participants->links() }}</div> --}}

    @endAdminTheme

@endsection