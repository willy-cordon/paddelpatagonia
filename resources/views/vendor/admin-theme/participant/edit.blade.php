@extends(config('admin-theme.master_layout'))

@section('content')

    @adminTheme('card', ['title' => 'Create Event'])

        @adminTheme('form', ['files' => true])

            <div class="row" style="margin:0px">
                <div class="col-md-6 col-sm-12">
                    @adminThemeSelect([
                        'name' => 'trip_id',
                        'label' => 'Trip',
                        'model' => $event,
                        'options' => \iVirtual\AdminTheme\AdminTheme::generateSelectOptions($trips, 'id', 'title')
                    ])
                    @push('scripts')
                    <script type="text/javascript">
                        var trips = '{!! $trips->toJson() !!}';

                        function findTripByKey(array, key, value) {
                            for (var i = 0; i < array.length; i++) {
                                if (array[i][key] === value) {
                                    return array[i];
                                }
                            }
                            return null;
                        }

                        $("select[name='trip_id']").on('change', function () {
                            $('input[name="price"]').val(findTripByKey(JSON.parse(trips), 'id', parseInt(this.value)).price).change()
                        });
                    </script>
                    @endpush
                </div>
                <div class="col-md-3 col-sm-12">
                    @adminThemeInput(['name' => 'price', 'model' => $event, 'label' => 'Price', 'type' => 'number'])
                </div>
                <div class="col-md-3 col-sm-12">
                    @adminThemeDate(['name' => 'date', 'model' => $event, 'label' => 'Date', 'format' => 'Y-m-d'])
                </div>
                <div class="col-md-3 col-sm-12">
                    @adminThemeInput(['name' => 'duration', 'model' => $event, 'label' => 'Duration in days', 'type' => 'number'])
                </div>
                <div class="col-md-3 col-sm-12">
                    @adminThemeInput(['name' => 'availability', 'model' => $event, 'label' => 'Total availability', 'type' => 'number'])
                </div>
                <div class="col-md-6 col-sm-12">
                    @adminThemeInput(['name' => 'starting_point', 'model' => $event, 'label' => 'Starting point'])
                </div>
            </div>

            @adminThemeTextarea(['name' => 'summary', 'model' => $event, 'label' => 'Summary', 'editor' => true])

        @endAdminTheme

    @endAdminTheme

@endsection