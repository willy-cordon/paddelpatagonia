@extends(config('admin-theme.master_layout'))

@section('content')

    @if(session()->has('status'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    @endif

    @adminTheme('card', ['title' => 'View Trips'])


        @slot('actions')

            @adminThemeButton(['text' => 'Create Trip', 'url' => url()->route('trips.create')])

        @endslot


        @adminTheme('table', ['actions' => '25'])

            @slot('head')

                <th class="text-center">Image</th>

                <th>Title</th>

                <th>Price</th>

            @endslot

            @foreach($trips as $trip)

                <tr>

                    <td class="text-center">
                        <img src="{{ $trip->imageUrl() }}" height="50" width="50"/>
                    </td>

                    <td>{{ $trip->title }}</td>

                    <td>{{ $trip->formated_price }}</td>

                    <td class="text-center">

                        @adminThemeButton([
                            'url' => url()->route('singleTrip', ['slug' => $trip->slug]),
                            'icon' => 'remove_red_eye',
                            'style' => 'flat',
                            'size' => 'sm',
                            'hover' => 'View'
                        ])


                        @adminThemeButton([
                            'url' => url()->route('trips.edit', ['id' => $trip->id]),
                            'icon' => 'edit',
                            'style' => 'flat',
                            'size' => 'sm',
                            'hover' => 'Update'
                        ])


                        @adminThemeDeletePopup([
                            'id' => $trip->id,
                            'url' => url()->route('trips.delete', ['id' => $trip->id]),
                        ])

                    </td>

                </tr>

            @endforeach

        @endAdminTheme

        <div style="float: right">{{ $trips->links() }}</div>

    @endAdminTheme

@endsection