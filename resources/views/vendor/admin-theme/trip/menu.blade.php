
@can('manage-trips')

    @adminTheme('menu_item', ['icon' => 'terrain'])

        Trips

        @slot('children')

            @adminTheme('menu_item', ['url' => url()->route('trips.index')])

            View trips

            @endAdminTheme


            @adminTheme('menu_item', ['url' => url()->route('trips.create')])

            Create trip

            @endAdminTheme


        @endslot

    @endAdminTheme

@endcan