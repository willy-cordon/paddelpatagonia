@extends(config('admin-theme.master_layout'))

@section('content')

    @adminTheme('card', ['title' => 'Create Trip' . $trip->title])

        @adminTheme('form', ['files' => true])
        <input type="hidden" name="id" value="{{ $trip->id }}">

            <div class="row" style="margin:0px">
                <div class="col-md-9 col-sm-12">
                    @adminThemeInput(['name' => 'title', 'model' => $trip, 'label' => 'Trip Title'])
                </div>
                <div class="col-md-3 col-sm-12">
                    @adminThemeInput(['name' => 'price', 'model' => $trip, 'label' => 'Price', 'type' => 'number'])
                </div>
                <div class="col-sm-12">
                    <div class="form-group pmd-textfield pmd-textfield-floating-label{{ $errors->has('image') ? ' has-error' : '' }}">
                    <h4>Upload image</h4>
                    <input type="file" name="image">
                    @if ($errors->has('image'))
                        <span class="pmd-textfield-focused"></span>
                        <p class="help-block">{{ $errors->first('image') }}</p>
                    @endif
                     </div>
                </div>
            </div>

            @adminThemeTextarea(['id' => 'body', 'name' => 'body', 'model' => $trip, 'label' => 'Description', 'editor' => true])
            @adminThemeTextarea(['id' => 'itinerary', 'name' => 'itinerary', 'model' => $trip, 'label' => 'Itinerary', 'editor' => true])
            @adminThemeTextarea(['id' => 'accomodations', 'name' => 'accomodations', 'model' => $trip, 'label' => 'Accomodations', 'editor' => true])
            @adminThemeTextarea(['id' => 'logistinc', 'name' => 'logistic', 'model' => $trip, 'label' => 'Logistics', 'editor' => true])

        @endAdminTheme

    @endAdminTheme

@endsection