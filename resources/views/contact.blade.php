@extends('layouts.master')
@section('title')
    Contact - PaddelPatagonia
@endsection

@section('titulo')
    Contact
@endsection

@section('content')
@include('componets.headPage')


<div class="container">
    <div class="row">
        <div class="col-md-8 contactleft">
            <h2 class="text-center  ">If you need, Just drop us a line</h2>

            <p>Thank-you for your interest in our trips and courses! The best way to contact us is by email. We will endeavor to respond within 24hrs but please be patient if it takes us a bit longer as sometimes we find ourselves in places without cell or internet service (and these places are usually really awesome!)</p>
            <p>We would also be happy to chat with you by phone or skype. Give us a call or better yet, let’s set up a time in advance to talk about your future adventure in Patagonia!</p>




                <form class="contact-form" method="post" action="#">
						<div class="row columns_padding_5">

							<div class="col-xs-12 col-md-6 ">
								<div class="filed_form">
									<input type="text" value="" name="name" class="form-control" required placeholder="Name">
								</div>
							</div>
							<div class="col-xs-12 col-md-6">
								<div class="filed_form">
									<input type="tel" value="" name="phone" class="form-control" required placeholder="Phone">
								</div>
							</div>
							<div class="col-xs-12 col-md-6">
								<div class="filed_form">
									<input type="email" value="" name="email" class="form-control" required placeholder="Email">
								</div>
							</div>
							<div class="col-xs-12 col-md-6">
								<div class="filed_form">
									<input type="text" value="" name="subject" class="form-control" placeholder="Topic">
								</div>
							</div>
							<div class="col-xs-12 col-md-12">
								<div class="filed_form">
									<textarea rows="4" cols="45" name="message" placeholder="Message"></textarea>
								</div>
							</div>

						</div>
						<div class="contact_form_submit">
							<button type="submit" name="contact_submit" class="sv_btn sv_btn_default">Submit</button>
						</div>
                    </form>
            </div>
                <div class="col-md-4 infocontact">
                <h2 class="text-center ">Katie & Julian Tisato</h2>
                    <p><i class="fas fa-map-marked-alt"></i> El Bolsón, Río Negro, Argentina</p>
                    <hr>
                    <p><i class="fas fa-phone-square"></i> (+54) 2944 274238</p>
                    <hr>
                    <p><i class="fas fa-at"></i> info@paddlepatagonia.com</p>
                    <hr>
                    <p><i class="fas fa-envelope"></i> paddlepatagonia@gmail.com</p>
                    <hr>
                    
                    <a href="#"><i class="fab fa-youtube"></i></a> <a href="#"><i class="fab fa-facebook-square"></i></a> <a href="#"><i class="fab fa-skype"></i></a>
                </div>
                </div>

        </div>



@endsection
