@extends('layouts.master')

@section('title')
Events - Paddle Patagonia
@endsection
@section('titulo')
    {{$event->trip->title}}
@endsection

@section('content')
@include('componets.headPage')

	<div class="type_page_wrapper section_pad">
        <div class="container">
            <div class="row content">
                <div class="col-md-4 col-sm-12 col-xs-12 date">
                        <h2 class="text-center text-muted"><i class="fas fa-calendar-alt"></i> {{$event->formated_date}}</h2>
                </div>
                <div class="col-md-8 col-sm-12 col-xs-12 price">
                       <h2 class="text-center text-muted">{{$event->formated_price}}</h2>
                </div>
            </div>
        </div>


		<div class="container">

			<div class="row">
				<div class="image-column col-md-8 col-sm-12 col-xs-12">
					<div class="preview_post_image">
						<a href="{{ $event->trip->getFirstMediaUrl('gallery', 'fullhd') }}" class="popup_image hover_link_effect"><i class="fas fa-search"></i><img src="{{ $event->trip->getFirstMediaUrl('gallery', 'big') }}" alt="{{ $event->trip_title }}"></a>
					</div>
					<ul class="card_detail_info">
						<li><strong>Duration:</strong> {{$event->duration}} days</li>
						<li><strong>Availability:</strong> {{$event->availability}} places </li>
						<li><strong>Starting Point:</strong> {{$event->starting_point}}</li>
					</ul>
				</div>
				<div class="info-column col-md-4 col-sm-12 col-xs-12">

					<div class="entry_header">
                        {!! $event->summary !!}
					</div>

					<div class="entry_content">

					</div>

					<form class="contact-form" method="post" action="#">
						<div class="row columns_padding_5">

							<div class="col-xs-12 col-md-6 ">
								<div class="filed_form">
									<input type="text" value="" name="name" class="form-control" required placeholder="Name">
								</div>
							</div>
							<div class="col-xs-12 col-md-6">
								<div class="filed_form">
									<input type="tel" value="" name="phone" class="form-control" required placeholder="Phone">
								</div>
							</div>
							<div class="col-xs-12 col-md-6">
								<div class="filed_form">
									<input type="email" value="" name="email" class="form-control" required placeholder="Email">
								</div>
							</div>
							<div class="col-xs-12 col-md-6">
								<div class="filed_form">
									<input type="text" value="" name="subject" class="form-control" placeholder="Topic">
								</div>
							</div>
							<div class="col-xs-12 col-md-12">
								<div class="filed_form">
									<textarea rows="4" cols="45" name="message" placeholder="Message"></textarea>
								</div>
							</div>

						</div>
						<div class="contact_form_submit">
							<button type="submit" name="contact_submit" class="sv_btn sv_btn_default">Submit</button>
						</div>
					</form>

				</div>
			</div>
		</div>
    </div>

<!--type page wrapper-->
<div class="type_page_wrapper section_padding_60_60">

        <!--Start container-->
        <div class="container">

            <div class="row">

                <div class="col-xs-12 col-md-12 col-lg-8">

                    <div class="post_preview_image">
                        <a href="assets/images/gallery/default-slide.jpg" class="see_big_pic hover_link_effect">
                            <i class="fas fa-search"></i>
                            <img src="assets/images/gallery/default-slide.jpg" alt="">
                        </a>
                    </div>

                    <div class="item_content">

                        <p class="text_indent_30">{!! $event->trip->body !!}</p>
                            <br>


                        <br>
                        <blockquote>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, ipsam tempora necessitatibus at voluptas repudiandae error asperiores sit vero recusandae.
                        </blockquote>
                        <br>
                        <div>
                            {!!$event->trip->accomodations!!}
                        </div>
                        <div>{!!$event->trip->logistic!!}</div>
                    </div>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="sidebar_right">
                        <!-- sidebar-block -->

                        <!-- sidebar-block -->
                        <div class="sidebar_block light_area">
                            <div class="widget_tweet_list">
                                <h3 class="widget_title">Simple Itinerary</h3>

                                <div>
                                   {!!$event->trip->itinerary!!}
                                </div>

                            </div>
                        </div><!-- END sidebar-block -->
                        <!-- sidebar-block -->


                    </div>
                </div>

            </div>


        </div><!--END Start container-->

    </div><!--END type page wrapper-->
@endsection
