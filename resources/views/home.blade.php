@extends('layouts.master')

@section('title')
Paddle Patagonia – Whitewater Paddling in Patagonia
@endsection

@section('content')

	<div class="main_big_slider" id="main_slider1">
		<div class="owl-carousel main_b_slider">
			<!--Item big slider-->
			<div class="d-flex bg_img justify_content_center align_items_center item item_mainslide_1 bg_overlay1">

					<div class="container text-center">
						<h1 class="slider_big_title">
							Incredible Scenery
						</h1>

						<div class="slider_sub_title">
							<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam noempor invidunt<br> ut labore et dolore magna aliquyam erat</p>
						</div>
						<div class="text-center">
							<a href="#our_photo" class="sv_btn sv_btn_default easescroll">see more</a>
						</div>
					</div>

			</div><!--END Item big slider-->
			<!--Item big slider-->
			<div class="d-flex bg_img justify_content_center align_items_center item item_mainslide_2 bg_overlay1">

					<div class="container text-center">
						<h1 class="slider_big_title">
							Blue Lake
						</h1>
						<div class="slider_sub_title">
							<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam noempor invidunt<br> ut labore et dolore magna aliquyam erat</p>
						</div>
						<div class="text-center">
							<a href="#our_photo" class="sv_btn sv_btn_default easescroll">see more</a>
						</div>
					</div>

			</div><!--END Item big slider-->
			<!--Item big slider-->
			<div class="d-flex bg_img justify_content_center align_items_center item item_mainslide_3 bg_overlay1">

					<div class="container text-center">
						<h1 class="slider_big_title">
							Camping Morning
						</h1>
						<div class="slider_sub_title">
							<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam noempor invidunt<br> ut labore et dolore magna aliquyam erat</p>
						</div>
						<div class="text-center">
							<a href="#our_photo" class="sv_btn sv_btn_default easescroll">see more</a>
						</div>
					</div>

			</div><!--END Item big slider-->
		</div>
	</div><!--END main_big_carousel -->
	<!-- advatnages_list -->
	<div class="advatnages_list light_area1  nexEvents">
		<div class="container" id="blogCarousel">
			<div class="title1">
				<h2>Upcoming <span>Trips</span> </h2>
			</div>

			<div class="row">
				@foreach($events as $event)
				<div class="col-lg-3 col-md-4">
					<ul class="conten1">
						<li>
							<div class="boxdata">
								<div class="DateEvent"><i class="far fa-calendar-alt"></i>  {{ $event->formated_date }}</div>
								<h6 class="NameEvent">{{ $event->trip_title }}</h6>
							</div>

							<div class="imgEvent">
									<img src="{{ $event->trip->getFirstMediaUrl('gallery', 'thumb') }}" alt="{{ $event->trip_title }}">
									<div class="boton">
									<a href="/events/{{$event->slug}}" class="more">see more</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
				@endforeach

			</div>

			</div>
		</div>
	<!-- about_main -->
	<section class="about_main" id="about">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 about_text_main no_padding">
					<h2>
						21 Years of Experiences<br>
						In Tourism Business
					</h2>

					<p>
						There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. </p>
					<p class="text_indent_30">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.
					There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.  </p>
					<h5>Advantages list:</h5>
					<ul class="qub_list">
						<li>Entertainment in the mountains</li>
						<li>The best views</li>
						<li>Cycling</li>
						<li>Hike in the mountains</li>
						<li>Fishing</li>
					</ul>
				</div>
				<div class="col-lg-6 no_padding vide_block d-flex bg_img justify_content_center align_items_center bg_overlay1">

					<div class="vide_text_icon text-center">
						<a href="https://www.youtube.com/watch?v=yJHxXJEO-YQ" class="popup_video"><img src="assets/images/play-button.png" alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</section><!--END about_main -->


	<section class="our_photos section_padding_100_0" id="our_photo" >
		<div class="container">
			<div class="section_heading text-center">
				<h2 class="section_heading_title">Our Photos</h2>
				<div class="describe_section_title">
					<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam noempor invidunt<br> ut labore et dolore magna aliquyam erat</p>
				</div>
			</div>
		</div>
		<div class="container-fluid">

			<!-- Filter navigation -->
			<div class="category-control text-center">
				<button class="filter sv_btn sv_btn_trnsp active" data-group="all">All</button>
				<button class="filter sv_btn sv_btn_trnsp" data-group="fishing">Fishing</button>
				<button class="filter sv_btn sv_btn_trnsp" data-group="rooms">Rooms</button>
				<button class="filter sv_btn sv_btn_trnsp" data-group="kayaking">Kayaking</button>
				<button class="filter sv_btn sv_btn_trnsp" data-group="cycling">Cycling</button>
				<button class="filter sv_btn sv_btn_trnsp" data-group="camping">Camping</button>
			</div><!--END Filter navigation -->

			<div class="row grid_pics">
				<!-- item mixi card -->
				<div class="col-md-3 col-sm-6 no_padding item mix" data-groups=["fishing"]>
					<img src="assets/images/gallery/small1.jpg" alt="">
					<div class="grid-item-wrapp">
						<div class="psev_table">
							<div class="psev_table_row">
								<div class="project_title">
									<a href="assets/images/gallery/large1.jpg" class="see_big_pic"><i class="fas fa-search"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>	<!--END item mixi card -->
				<!-- item mixi card -->
				<div class="col-md-3 col-sm-6 no_padding item mix" data-groups=["rooms"]>
					<img src="assets/images/gallery/small2.jpg" alt="">
					<div class="grid-item-wrapp">
						<div class="psev_table">
							<div class="psev_table_row">
								<div class="project_title">
									<a href="assets/images/gallery/large2.jpg" class="see_big_pic"><i class="fas fa-search"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>	<!--END item mixi card -->
				<!-- item mixi card -->
				<div class="col-md-3 col-sm-6 no_padding item mix" data-groups=["kayaking"]>
					<img src="assets/images/gallery/small3.jpg" alt="">
					<div class="grid-item-wrapp">
						<div class="psev_table">
							<div class="psev_table_row">
								<div class="project_title">
									<a href="assets/images/gallery/large3.jpg" class="see_big_pic"><i class="fas fa-search"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>	<!--END item mixi card -->
				<!-- item mixi card -->
				<div class="col-md-3 col-sm-6 no_padding item mix" data-groups=["kayaking"]>
					<img src="assets/images/gallery/small4.jpg" alt="">
					<div class="grid-item-wrapp">
						<div class="psev_table">
							<div class="psev_table_row">
								<div class="project_title">
									<a href="assets/images/gallery/large4.jpg" class="see_big_pic"><i class="fas fa-search"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>	<!--END item mixi card -->
				<!-- item mixi card -->
				<div class="col-md-3 col-sm-6 no_padding item mix" data-groups=["cycling"]>
					<img src="assets/images/gallery/small5.jpg" alt="">
					<div class="grid-item-wrapp">
						<div class="psev_table">
							<div class="psev_table_row">
								<div class="project_title">
									<a href="assets/images/gallery/large5.jpg" class="see_big_pic"><i class="fas fa-search"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>	<!--END item mixi card -->
				<!-- item mixi card -->
				<div class="col-md-3 col-sm-6 no_padding item mix" data-groups=["rooms"]>
					<img src="assets/images/gallery/small6.jpg" alt="">
					<div class="grid-item-wrapp">
						<div class="psev_table">
							<div class="psev_table_row">
								<div class="project_title">
									<a href="assets/images/gallery/large6.jpg" class="see_big_pic"><i class="fas fa-search"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>	<!--END item mixi card -->
				<!-- item mixi card -->
				<div class="col-md-3 col-sm-6 no_padding item mix" data-groups=["camping"]>
					<img src="assets/images/gallery/small7.jpg" alt="">
					<div class="grid-item-wrapp">
						<div class="psev_table">
							<div class="psev_table_row">
								<div class="project_title">
									<a href="assets/images/gallery/large7.jpg" class="see_big_pic"><i class="fas fa-search"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>	<!--END item mixi card -->
				<!-- item mixi card -->
				<div class="col-md-3 col-sm-6 no_padding item mix" data-groups=["cycling"]>
					<img src="assets/images/gallery/small8.jpg" alt="">
					<div class="grid-item-wrapp">
						<div class="psev_table">
							<div class="psev_table_row">
								<div class="project_title">
									<a href="assets/images/gallery/large8.jpg" class="see_big_pic"><i class="fas fa-search"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>	<!--END item mixi card -->
				<!-- item mixi card -->
				<div class="col-md-3 col-sm-6 no_padding item mix" data-groups=["rooms"]>
					<img src="assets/images/gallery/small9.jpg" alt="">
					<div class="grid-item-wrapp">
						<div class="psev_table">
							<div class="psev_table_row">
								<div class="project_title">
									<a href="assets/images/gallery/large9.jpg" class="see_big_pic"><i class="fas fa-search"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>	<!--END item mixi card -->
				<!-- item mixi card -->
				<div class="col-md-3 col-sm-6 no_padding item mix" data-groups=["cycling"]>
					<img src="assets/images/gallery/small10.jpg" alt="">
					<div class="grid-item-wrapp">
						<div class="psev_table">
							<div class="psev_table_row">
								<div class="project_title">
									<a href="assets/images/gallery/large10.jpg" class="see_big_pic"><i class="fas fa-search"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>	<!--END item mixi card -->
				<!-- item mixi card -->
				<div class="col-md-3 col-sm-6 no_padding item mix" data-groups=["fishing"]>
					<img src="assets/images/gallery/small11.jpg" alt="">
					<div class="grid-item-wrapp">
						<div class="psev_table">
							<div class="psev_table_row">
								<div class="project_title">
									<a href="assets/images/gallery/large11.jpg" class="see_big_pic"><i class="fas fa-search"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>	<!--END item mixi card -->
				<!-- item mixi card -->
				<div class="col-md-3 col-sm-6 no_padding item mix" data-groups=["camping"]>
					<img src="assets/images/gallery/small12.jpg" alt="">
					<div class="grid-item-wrapp">
						<div class="psev_table">
							<div class="psev_table_row">
								<div class="project_title">
									<a href="assets/images/gallery/large12.jpg" class="see_big_pic"><i class="fas fa-search"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>	<!--END item mixi card -->

			</div>

		</div>
	</section><!--END our_photos -->
	<!--contenido nuevo -->
	<section class="news_list light_area section_padding_100_100">
			<div class="container">
				<div class="section_heading text-center">
					<h2 class="section_heading_title title2">latest  <span>News</span>  </h2>
					<div class="describe_section_title">
						<p>
							You can checking out our latest news every days.<br>
							We update our proposals and news.
						<p-->
					</div>
				</div>
				<!-- latest news carousel -->
				<div class="owl-carousel latest_news_carousel sv_dots_nav">

					<div class="item"><!--new item-->
						<h5 class="new_name"><a href="#">Good Weather</a></h5>
						<div class="new_date"><i class="far fa-calendar-alt"></i> 27 APR, 2018</div>
						<p>
							Lorem ipsum dolor sit amet, consectetur
							adipiscing elit. In risus magna, porta sit amet
							orci ultrices, condimentum <a href="#" class="reed_more"><i class="far fa-hand-pointer"></i>Read More...</a>
						</p><!--END new item-->
					</div>

					<div class="item"><!--new item-->
						<h5 class="new_name"><a href="#">Special Program</a></h5>
						<div class="new_date"><i class="far fa-calendar-alt"></i> 12 APR, 2018</div>
						<p>
							Lorem ipsum dolor sit amet, consectetur
							adipiscing elit. In risus magna, porta sit amet
							orci ultrices, condimentum <a href="#" class="reed_more"><i class="far fa-hand-pointer"></i>Read More...</a>
						</p><!--END new item-->
					</div>

					<div class="item"><!--new item-->
						<h5 class="new_name"><a href="#">New Cycling Roads</a></h5>
						<div class="new_date"><i class="far fa-calendar-alt"></i> 03 APR, 2018</div>
						<p>
							Lorem ipsum dolor sit amet, consectetur
							adipiscing elit. In risus magna, porta sit amet
							orci ultrices, condimentum <a href="#" class="reed_more"><i class="far fa-hand-pointer"></i>Read More...</a>
						</p><!--END new item-->
					</div>

					<div class="item"><!--new item-->
						<h5 class="new_name"><a href="#">Fishing for Youngest</a></h5>
						<div class="new_date"><i class="far fa-calendar-alt"></i> 21 MAR, 2018</div>
						<p>
							Lorem ipsum dolor sit amet, consectetur
							adipiscing elit. In risus magna, porta sit amet
							orci ultrices, condimentum <a href="#" class="reed_more"><i class="far fa-hand-pointer"></i>Read More...</a>
						</p><!--END new item-->
					</div>

					<div class="item"><!--new item-->
						<h5 class="new_name"><a href="#">Season Discounts</a></h5>
						<div class="new_date"><i class="far fa-calendar-alt"></i> 09 MAR, 2018</div>
						<p>
							Lorem ipsum dolor sit amet, consectetur
							adipiscing elit. In risus magna, porta sit amet
							orci ultrices, condimentum <a href="#" class="reed_more"><i class="far fa-hand-pointer"></i>Read More...</a>
						</p><!--END new item-->
					</div>

					<div class="item"><!--new item-->
						<h5 class="new_name"><a href="#">Personal Photographer</a></h5>
						<div class="new_date"><i class="far fa-calendar-alt"></i> 05 MAR, 2018</div>
						<p>
							Lorem ipsum dolor sit amet, consectetur
							adipiscing elit. In risus magna, porta sit amet
							orci ultrices, condimentum <a href="#" class="reed_more"><i class="far fa-hand-pointer"></i>Read More...</a>
						</p><!--END new item-->
					</div>

				</div><!-- END latest news carousel -->
			</div>
		</section><!-- END news_list -->

	<!-- review_form -->
	<section class="review_form_line bg_overlay1 section_padding_100_100" id="review_form">
		<div class="container">
			<!-- reviews carousel -->
			<div class="owl-carousel reviews_carousel sv_dots_nav">
				<!-- review item -->
				<div class="item">
					<div class="photo_review">
						<img src="assets/images/person-flat.png" alt="">
					</div>
					<h4 class="name_review">
						Joshua and Marylin
					</h4>
					<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need.</p>

				</div><!--END review item -->
				<!-- review item -->
				<div class="item">
					<div class="photo_review">
						<img src="assets/images/person-flat.png" alt="">
					</div>
					<h4 class="name_review">
						Cristy
					</h4>
					<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need.</p>

				</div><!--END review item -->
				<!-- review item -->
				<div class="item">
					<div class="photo_review">
						<img src="assets/images/person-flat.png" alt="">
					</div>
					<h4 class="name_review">
						Bob and Sky
					</h4>
					<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need.</p>

				</div><!--END review item -->

			</div><!--END reviews slider -->

		</div>
	</section><!-- END review_form -->

@endsection

@section('scripts')
	<script src="/assets/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
@endsection
