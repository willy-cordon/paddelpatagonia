<div style="{{ isset($image) ? 'background-image:url('.$image.')' : '' }}"" class="header_title pade_bg2 d-flex justify_content_center align_items_center bg_overlay1">
    <div class="container">
        <h1 class="page_title">@yield('titulo')</h1>

    </div>
</div>
