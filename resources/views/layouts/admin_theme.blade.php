@extends('admin-theme::layouts.admin')

@section('admin-theme-section-menu')

    @include('admin-theme::user.menu')
    @include('admin-theme::trip.menu')
    @include('admin-theme::event.menu')

@endsection

@section('admin-theme-section-content')
    @yield('content')

@endsection