@extends('layouts.master')

@section('title')
Events - Paddle Patagonia
@endsection
@section('titulo')
    Upcoming Events
@endsection

@section('content')
@include('componets.headPage')


    <section class="feature-area section-gap" id="secvice">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="menu-content pb-60 col-lg-8">
                        <div class="title text-center">
                            <h2 class="mb-10">All available events</h2>
                            <p>Who are in extremely love with eco friendly system.</p>
                        </div>
                    </div>
                </div>
                <div class="row definition">
                    @foreach ($events as $event)

                <a href="{{ route('singleEvent', ['slug' => $event->slug]) }}">
                    <div class="col-sm-4">
                        <div class="single-feature mb-30 contentevent">
                            <div class="d-flex flex-row pb-20 headerevent">
                            <h4 class="text-center">{{$event->trip->title}}</h4>
                            </div>
                            <hr>
                            <div class="row pricedate">
                                <div class="col-8 col-sm-6">
                                    <span>{{$event->formated_date}}</span>
                                </div>
                                <div class="col-4 col-sm-6">
                                    <span>{{$event->trip->formated_price}}</span>
                                </div>
                            </div>
                            <p>
                                    {{$event->summary}}
                            </p>
                        </div>
                    </a>
                    </div>
                     @endforeach
                     @if ($events->count())
                    <div class="paginator">
                        {{$events->links()}}
                    </div>

                    @endif

                </div>
            </div>
        </section>





@endsection





