@extends('layouts.master')
@section('title')

Adventure Trips - Paddel Patagonia
@endsection
@section('titulo')
    {{ $trip->title }}
@endsection
@section('content')
@component('componets.headPage', ['image' => $trip->getFirstMediaUrl('gallery', 'fullhd')])
@endcomponent


    <!--type page wrapper-->
	<div class="type_page_wrapper section_padding_60_60">

            <!--Start container-->
            <div class="container">

                <div class="row">

                    <div class="col-xs-12 col-md-12 col-lg-8">

                        <div class="post_preview_image">
                            <a href="/assets/images/gallery/default-slide.jpg" class="see_big_pic hover_link_effect">
                                <i class="fas fa-search"></i>
                                <img src="/assets/images/gallery/default-slide.jpg" alt="">
                            </a>
                        </div>

                        <div class="item_content">

                        <h2 class="entry-title">Estimated Price: <br> {{$trip->formated_price}}</h2>

                            <p class="text_indent_30">{!! $trip->body !!}</p>
                                <br>


                            <br>
                            <h5>Accomodations</h5>
                            <blockquote>
                                {!!$trip->accomodations!!}
                            </blockquote>
                            <br>
                            <div>
                            </div>
                            <div>
                            <h5>Logistics</h5>
                            {!!$trip->logistic!!}</div>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                        <div class="sidebar_right">
                            <!-- sidebar-block -->

                            <!-- sidebar-block -->
                            <div class="sidebar_block light_area">
                                <div class="widget_tweet_list">
                                    <h3 class="widget_title">Simple Itinerary</h3>

                                    <div>
                                       {!!$trip->itinerary!!}
                                    </div>

                                </div>
                            </div><!-- END sidebar-block -->
                            <!-- sidebar-block -->


                        </div>
                    </div>

                </div>


            </div><!--END Start container-->

        </div><!--END type page wrapper-->





@endsection
