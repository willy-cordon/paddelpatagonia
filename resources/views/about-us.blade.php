@extends('layouts.master')
@section('title')
    AboutUs - PaddelPatagonia
@endsection

@section('titulo')
    About Us
@endsection

@section('content')
@include('componets.headPage')





    <div class="container">
        <div class="row">
        <div class="col-md-12 textwhite">
            <h2 class="text-center">About The Area</h2>
                <h3>El Bolsón, Argentina</h3>

                <p>El Bolsón is Paddle Patagonia’s home base. It is a small town located in the province of Rio Negro about 1800km south of Buenos Aires. You can take a bus (about 22 hours but very comfortable) or you can fly into San Carlos de Bariloche, then travel by land the 160km south to El Bolsón.</p>
                 <p>“El Bolsón” literally translates as “the big bag,” which is certainly not the most poetic name, but must come from the fact that El Bolsón is located in a valley between several mountains. This “bag” nestled in the mountains has an unusual microclimate for being so far south. The region produces 75% of the country’s hops and an enormous amount of organic fruits, vegetables and honey. I have never eaten more perfect and delicious strawberries in my life! On top of it all, locally produced cheeses, smoked trout, regional chocolate, jams and preserves make this place a food-lovers dream.</p>

        </div>

        </div>
        <div class="row">
            <div class="col-md-6 textwhite">
                <a href="/assets/images/bolson.jpg"><img src="/assets/images/bolson.jpg"  alt="Bolson- RioNegro - Argentina"></a>

            </div>
            <div class="col-md-6 textwhite">
                <p>
                 <p>The town is towered over by “Piltriquitron” mountain (2,284m) whose Mapuche name translates as a mountain “hanging from the clouds.” Across the valley to the east are many beautiful mountain peaks that offer a variety of moderate to challenging hikes.</p>
                 <p>Throughout the summer El Bolsón hosts the “Feria Artesanal” or local artisan craft and food fair. Tourists from all over the country flock to El Bolsón to enjoy the market with over 300 stalls to explore! Artists sell everything from sculpted wooden cutting boards, and handmade mate gourds to jewelry, instruments and pottery.</p>

                </p>
            </div>
        </div>

    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6 textwhite">
                <h3>Katie’s must-do list for El Bolsón:</h2>
                    <p>Organic Micro Brews from Cerveza Artesenal El Bolsón(Julian and I like the blondes, but the adventurous might try a “Cerveza Piqante” (infused with hot chilies) or the “Cerveza Frambuesa” (raspberry beer)
                        JauJa Ice-cream – organic ice-cream with no artificial additives and a plethora of flavours to choose! I vote chocolate espresso but others swear by the organic berry options.</p>
                        <p>Share a mate with friends– mate (pronounced mat-te) is made by steeping dried “yerba mate” leaves in a hollowed out gourd (the mate) and drank from a metal straw called a “bombilla.” This is a very popular and traditional tea shared amongst groups of friends or family. There is some etiquette involved in the process, which we will happily teach you upon arrival.</p>
                        <p>Visit the International Tango Festival in El Bolsón – This is an annual four day event that includes poetry, live music, dance, visual arts and tango, dance workshops, milongas and outdoor practices open to the community. This is a MUST-DO if you are in El Bolsón with the opportunity to see some of the most renowned tango masters from Argentina!</p>
            </div>
            <div class="col-md-6 textwhite" >
                <h3>San Carlos de Bariloche, Argentina</h3>
                <p>San Carlos de Bariloche is the “gateway city” to Patagonia, situated on the foothills of the Andes, surrounded by lakes (Nahuel Huapi, Gutiérrez Lake, Moreno Lake and Mascardi Lake) and mountains (Tronador, Cerro Catedral, Cerro López). It is famous for skiing but also great for sight-seeing, water sports, trekking and climbing. Another claim to fame is its Swiss-like atmosphere and its chocolate boutiques.</p>
                <img src="/assets/images/bari.jpg" alt="San Carlos de Bariloche">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 textwhite">
                <h3>Futaleufú, Chile</h3>
                 <img  src="/assets/images/fatu.jpg" alt="">
        </div>
        <div class="col-md-6 textwhite">
            <br>
            <br>
                <p>The town of Futaleufú, Chile is close to the Argentine border and has a population of about 2,000. The main income for the community is fly fishing, white water rafting, and tourism.</p>
                 <p>The Futaleufú River is a river fed by the lakes in the Los Alerces National Park in Chubut Province, Argentina, crossing the Andes Mountains and the international border into Chile and opening into the Yelcho Lake.</p>
        </div>

        </div>
    </div>


    <section class="our_team section_padding_60_60">
            <div class="container">
                <div class="section_heading text-center">
                    <h2 class="section_heading_title">Our Team</h2>
                    <div class="describe_section_title">
                        <p>Katie & Julian Tisato are the owner/operators of Paddle Patagonia, a small adventure tour company specializing in mountain biking, whitewater canoe and kayak trips, whitewater rescue courses and adventure vacations. Our home base is in the Patagonia region of Argentina just outside the small town of El Bolsón. Our trips are designed to combine an authentic Patagonian travel experience with the fun and adventurous things our guests love to do!</p>
                    </div>
                </div>
                <div class="owl-carousel team_carousel sv_arrow_nav">
                    <!-- team_card_item -->
                    <div class="team_card_item">
                        <div class="team_card_item_img">
                            <img src="assets/images/default_team.jpg" alt="">
                        </div>
                        <div class="name_card_wrapper">
                            <h4 class="team_card_item_name">
                                Julián
                            </h4>
                        </div>
                        <div class="overlay">
                            <h4 class="team_card_item_name">
                                <a href="single-team.html">Julián</a>
                            </h4>
                            <div class="short_text_team">
                                <h6>Service:</h6>
                                <p><a href="single-service.html">Cycling Guide</a></p>
                                <h6>Experiences:</h6>
                                <p>Julian is known as a talented and patient instructor as well as a skilled raft guide, kayaker and canoeist. He is a Wilderness First Responder as well as a Whitewater Rescue Technician Instructor with Boreal River Rescue and a Kayaking and Canoe Instructor with Paddle Canada.</p>
                            </div>
                            <div class="social_team">
                                <a href="#"><i class="fab fa-twitter-square"></i></a>
                                <a href="#"><i class="fab fa-facebook-square"></i></a>
                            </div>
                        </div>
                    </div><!-- END team_card_item -->
                    <!-- team_card_item -->
                    <div class="team_card_item">
                        <div class="team_card_item_img">
                            <img src="assets/images/default_team.jpg" alt="">
                        </div>
                        <div class="name_card_wrapper">
                            <h4 class="team_card_item_name">
                                Katie
                            </h4>
                        </div>
                        <div class="overlay">
                            <h4 class="team_card_item_name">
                                <a href="single-team.html">Katie</a>
                            </h4>
                            <div class="short_text_team">
                                <h6>Service:</h6>
                                <p><a href="single-service.html">Photographer</a></p>
                                <h6>Experiences:</h6>
                                <p>Katie always had a passion for outdoor adventure and especially international travel. During her years at the University of Guelph she spent time living and traveling in Germany, Turkey, France, England and Russia. Eventually graduating with an Honours degree in European Studies and following it up with a post-graduate degree in Eco-Adventure Tourism Management.</p>
                            </div>
                            <div class="social_team">
                                <a href="#"><i class="fab fa-twitter-square"></i></a>
                                <a href="#"><i class="fab fa-facebook-square"></i></a>
                            </div>
                        </div>
                    </div><!-- END team_card_item -->
                    <!-- team_card_item -->
                    <div class="team_card_item">
                        <div class="team_card_item_img">
                            <img src="assets/images/default_team.jpg" alt="">
                        </div>
                        <div class="name_card_wrapper">
                            <h4 class="team_card_item_name">
                                Fidel Moreno
                            </h4>
                        </div>
                        <div class="overlay">
                            <h4 class="team_card_item_name">
                                <a href="single-team.html">Fidel Moreno</a>
                            </h4>
                            <div class="short_text_team">
                                <h6>Service:</h6>
                                <p><a href="single-service.html">Kayaking Coach</a></p>
                                <h6>Experiences:</h6>
                                <p>Fidel comes from the small town of Trevelin, Argentina and he has been instructing whitewater kayaking and guiding paddling trips in Chile, Argentina and Canada for many years. He started out as a competitive swimmer but now spends (almost) every moment of his free time kayaking!. We are proud to say Fidel placed 2nd in the Pan-American Freestyle Kayaking championships in 2012.</p>
                            </div>
                            <div class="social_team">
                                <a href="#"><i class="fab fa-twitter-square"></i></a>
                                <a href="#"><i class="fab fa-facebook-square"></i></a>
                            </div>
                        </div>
                    </div><!-- END team_card_item -->
                    <!-- team_card_item -->
                    <div class="team_card_item">
                        <div class="team_card_item_img">
                            <img src="assets/images/default_team.jpg" alt="">
                        </div>
                        <div class="name_card_wrapper">
                            <h4 class="team_card_item_name">
                                Ezequiel Elizalde
                            </h4>
                        </div>
                        <div class="overlay">
                            <h4 class="team_card_item_name">
                                <a href="single-team.html">Ezequiel Elizalde</a>
                            </h4>
                            <div class="short_text_team">
                                <h6>Service:</h6>
                                <p><a href="single-service.html">Camping Guide</a></p>
                                <h6>Experiences:</h6>
                                <p>“Eze” was born and raised in the outskirts of El Bolsón where his family ran a campground on the banks of the Azul River.  As an adult he has been spending his summers guiding rafts on the river during the summer and instructing snowboarding in the winter.  He also works as an assistant instructor for Boreal River Rescue courses.</p>
                            </div>
                            <div class="social_team">
                                <a href="#"><i class="fab fa-twitter-square"></i></a>
                                <a href="#"><i class="fab fa-facebook-square"></i></a>
                            </div>
                        </div>
                    </div><!-- END team_card_item -->
                    <div class="team_card_item">
                        <div class="team_card_item_img">
                            <img src="assets/images/default_team.jpg" alt="">
                        </div>
                        <div class="name_card_wrapper">
                            <h4 class="team_card_item_name">
                                Mirko Moreno
                            </h4>
                        </div>
                        <div class="overlay">
                            <h4 class="team_card_item_name">
                                <a href="single-team.html">Mirko Moreno</a>
                            </h4>
                            <div class="short_text_team">
                                <h6>Service:</h6>
                                <p><a href="single-service.html">Camping Guide</a></p>
                                <h6>Experiences:</h6>
                                <p>I was born in Patagonia in the Welsh settlement town of Trevelin, surrounded by Andes mountains and rivers. Ever since I was a child, my parents gave me a sense of adventure. Now, I work as a full-time outdoor adventure guide, trip leading and doing logistics for multi-day adventures including trekking, whitewater kayaking, mountaineering, sea-kayaking and mountain biking.</p>
                            </div>
                            <div class="social_team">
                                <a href="#"><i class="fab fa-twitter-square"></i></a>
                                <a href="#"><i class="fab fa-facebook-square"></i></a>
                            </div>
                        </div>
                    </div><!-- END team_card_item -->
                </div>
            </div>
        </section><!-- END our_team -->



@endsection
