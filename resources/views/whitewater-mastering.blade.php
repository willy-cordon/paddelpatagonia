@extends('layouts.master')

@section('title')
whitewater-mastering
@endsection
@section('titulo')
    Whitewater Mastering
@endsection

@section('content')
@include('componets.headPage')
<div class="container">
    <div class="row">
        <div class="col-lg-8  about_text_main ">
            <h2>lorem title</h2>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sequi possimus delectus quod voluptatem, quis recusandae, nisi excepturi architecto repudiandae eius unde praesentium! Ex voluptatem tempora magni cupiditate maiores dolores eaque?</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci quam delectus odit, sequi quidem ipsa debitis? Veritatis veniam consequatur, pariatur totam doloribus necessitatibus mollitia, provident, ut porro explicabo vero dolore?</p>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laboriosam et odit repellendus est fugiat eum quaerat reprehenderit libero veniam illo. Possimus illo blanditiis rem recusandae dolorum maiores suscipit aliquam atque.</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Est accusantium vel quod veritatis! Velit temporibus provident sunt. Eveniet consequatur voluptatum cum, in facere corporis perspiciatis temporibus minus labore natus repudiandae.</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem odit suscipit ratione? Rem repudiandae reprehenderit quidem sed iusto eos, laboriosam perferendis corporis esse veniam quibusdam consequatur facere laudantium commodi alias.</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo numquam accusamus atque rem, consequuntur hic provident sed possimus consequatur perspiciatis unde minima earum sint nobis libero. Aliquam sed obcaecati necessitatibus!</p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
            <div class="sidebar_right">
                <!-- sidebar-block -->

                <div class="sidebar_block light_area">
                    <div class="widget_recent_post">
                        <h4 class="widget_title">Upcoming Events</h4>



                        <ul>
                         @foreach ($events as $event)
                            <li>
                                <div class="post_thumb">
                                    <a href="/events/{{$event->slug}}" class="hover_link_effect">
                                        <i class="fas fa-ellipsis-h"></i>
                                        <img src="{{ $event->trip->getFirstMediaUrl('gallery', 'thumb') }}" alt="">
                                    </a>
                                </div>
                                <div class="post_date"><i class="far fa-calendar-alt"></i>  {{$event->formated_date}}</div>
                                <h6 class="post_name"><a href="eventos.html">{{ $event->trip_title }}</a></h6>

                            </li>

                        @endforeach
                        </ul>

                    </div>
                </div><!-- END sidebar-block -->
                <!-- sidebar-block -->

                <!-- sidebar-block -->


            </div>
        </div>
    </div>
</div>
<!--EndInsert-->
<div id="paddling" class="jumbotron paral paralsec rel">

        <h2 class="display-3">WHITEWATER PADDLING</h2>

</div>
<section class="one">
<div class="container">
    <div class="row">
    <div class="col-md-12 textwhite">
            <h2>Whitewater Kayak, Canoe or Raft</h2>

            <p>We’ve always said that the beauty of whitewater paddling is that it can take you to places that many people will never see. The crystal clear water, rock formations and steep canyon walls hidden between a couple of rapids are only available to those who can maneuver a boat to get them there!</p>
             <p>The rivers we paddle in Patagonia are often accessed by driving down long stretches of dirt road with rarely a person in sight. A flock of sheep or cattle grazing along the road can be a more common sighting than the farmers who live in the centre of  huge expanses of land. To access different rivers we may travel through deserts, canyons and mountain terrains. The rivers we paddle are clean and refreshing, mostly fed by mountain lakes and glaciers.</p>

             <p> We have designed two levels of “Rivers of Patagonia” trips to give you an idea of the paddling options in the area.  However water levels at the time of year you choose to visit will determine which rivers are available to paddle. The best time of year for higher water levels in our area is November and early December but after that we can travel to other regions with high-volume rivers that are always running.</p>

            <p>We are happy to customize a trip to suit your group’s particular paddling interests and abilities and we also have many non-paddling activities to offer if you want to take a break from the rivers for a day or two.  On most rivers you can choose to canoe, kayak, inflatable kayak or raft.    Contact us to see your options and build the perfect paddling itinerary for your group!</p>
    </div>
    </div>
</div>

</section>
<!--tabs-->
<section class="two">
<div class="container">
    <div class="row">
        <div class="col-md-12 textwhite">
            <h2>Rivers of Patagonia II-III</h2>
        </div>

    </div>
    <div class="row">
        <div class="col-md-4 textwhite">
            <h4>Sample Itinerary:</h4>
            <ol>
                <li>Day 1: Arrival San Carlos de Bariloche</li>
                <li>Day 2: Azul or Foyel River, Argentina</li>
                <li>Day 3: Manso River, Argentina</li>
                <li>Day 4: Nanty Falls + Upper Corcovado River, Argentina</li>
                <li>Day 5: Lower Corcovado River, Argentina</li>
                <li>Day 6: Azul + Futaleufú Rivers , Chile</li>
                <li>Day 7: Futaleufú River , Chile</li>
                <li>Day 8: Return to San Carlos de Bariloche</li>
            </ol>
        </div>
        <div class="col-md-8 textwhite">
            <p>The Rivers of Patagonia Class II-III paddling trip is a unique exploration of a variety of rivers in both Argentina and Chile. Travelling from the “Swiss-inspired” town of San Carlos de Bariloche through rural villages further south you will get a first-hand authentic Patagonia experience.   This trip is suitable for confident class III canoeists and kayakers with a solid roll.</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam nam obcaecati natus fugit, dicta temporibus tempore a accusantium unde maxime quae earum debitis voluptatem alias, officiis amet distinctio vitae nesciunt!</p>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Earum a aliquid provident? Similique repudiandae laboriosam quae qui placeat deleniti temporibus culpa! Aspernatur culpa fugiat illum mollitia deleniti id, iste quam.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 textwhite">
            <h2>Rivers of Patagonia III-IV
            </h2>
        </div>

    </div>
    <div class="row">
        <div class="col-md-4 textwhite">
            <h4>Sample Itinerary:</h4>
            <ol>
                <li>Day 1: Arrival San Carlos de Bariloche</li>
                <li>Day 2: Azul or Foyel River, Argentina</li>
                <li>Day 3: Manso River, Argentina</li>
                <li>Day 4: Nanty Falls + Upper Corcovado River, Argentina</li>
                <li>Day 5: Lower Corcovado River, Argentina</li>
                <li>Day 6: Azul + Futaleufú Rivers , Chile</li>
                <li>Day 7: Futaleufú River , Chile</li>
                <li>Day 8: Return to San Carlos de Bariloche</li>
            </ol>
        </div>
        <div class="col-md-8 textwhite">
            <p>On a class III-IV Rivers of Patagonia trip we can explore rivers in both Argentina and Chile. Along the way you will also experience the culture and beauty of Patagonia. A sample journey may begin on the rivers in the foothills of the Andes Mountain range. Continuing south, we paddle canyons that cut deep into the flatlands of the “Esteppe” before we cross the Andes into Chile and paddle the immense waters of the world-renowned Futaleufú River.</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam nam obcaecati natus fugit, dicta temporibus tempore a accusantium unde maxime quae earum debitis voluptatem alias, officiis amet distinctio vitae nesciunt!</p>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Earum a aliquid provident? Similique repudiandae laboriosam quae qui placeat deleniti temporibus culpa! Aspernatur culpa fugiat illum mollitia deleniti id, iste quam.</p>
        </div>
    </div>
    <!--Acordeon-->
    <section class="two">
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <div id="accordion" class="panel-group">
                          <div class="panel">
                            <div class="panel-heading">
                            <h4 class="panel-title">
                              <a href="#sample" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"> Sample Itinerary</a>
                              </h4>
                            </div>
                            <div id="sample" class="panel-collapse collapse in" >
                            <div class="panel-body cart2">
                                    <p><strong>Day 1:</strong>
                                        Welcome to San Carlos de Bariloche the gateway to Patagonia! There are a few flights a day into Bariloche and once everyone has arrived we begin our journey south via the scenic “circuito chico” passing through the famous Lake District including Lago Nahuel Huapi and Lago Guillermo towered over by the Andes mountain range. Just before reaching the town of El Bolsón we will turn off the highway and start making our way down an exciting 4×4 road, arriving in time for some vino Argentino and a hearty dinner at our remote eco-lodge nestled in the mountains. Dinner is included.</p>
                                <p><strong>Day 2:</strong>
                                        After a hearty breakfast at the lodge we outfit our boats and head to the first river of the week – found right at the lodge’s doorstep. (We should mention that the doorstep is 163 stairs tall!) When we reach the crystal clear waters of the Azul at the bottom, we guarantee you will feel that every step was worth it. Here we put on for a fun warm-up class II-III section. At the end of this river day – and every day at the lodge – we come back for happy hour, an optional yoga class, wood-fired sauna and hot tubs overlooking the river gorge.</p>
                                <p><strong>Day 3:</strong>
                                        After breakfast this morning we will hop back into our 4×4 vehicle to get us out of the Azul River valley to head north to do some paddling inside the Nahuel Haupí National Park. We’ll paddle this high volume river, running class III lines in crystal-clear turquoise water with some good surfing opportunities. The river takes us right up to the Chilean border – but we are not crossing today because we’ve got a few more gems to show you in Argentina first! Lunch on the river and back to the lodge at the end of the day for happy hour, yoga, wood-fired sauna and hot tubs. Dinner at lodge.</p>
                                <p><strong>Day 4:</strong>
                                        Today we will leave the lodge and head to the Corcovado River. As we head towards Corcovado we will notice the transition between the “precordillera” mountains (the foothills of the Andes) and the Patagonian “Esteppe” (semi-arid flatlands). The small town of Corcovado is our last stop in Argentina before crossing to Chile. If water levels are right we will stop en route to try our hand at running the fun Nanty Falls waterfall, located inside a private “Estancia” – you can run it as many times as you like, we will give you tips but not to worry no waterfall experience is necessary! After lunch we’ll keep on moving so that we can fit in a short run of the class II+ Upper Corcovado River. This is a popular rafting river and we often come across fishermen casting for rainbow trout and salmon.</p>
                                 <p><strong>Day 5:</strong>
                                        After an early breakfast and packed bags this morning we will head to the lesser-known lower section of the Corcovado River. This is a class II-III section that takes us into a beautiful class III canyon. At the end of this river we hop in the vehicles and head straight for the Chilean border, crossing at Palena. From here we drive to our cabin accommodations for the next few days at the hidden Campo Tres Monjas located at the confluence of the mighty Futaleufú and Azul Rivers. Lunch on the river, dinner at Campo Tres Monjas.</p>
                                <p><strong>Day 6:</strong>
                                        We’ll have a big breakfast at camp before heading out to do a short run on the Azul River where you will get to test your skills to the max! The Azul is a steep continuous class III run that takes us right back to Campo Tres Monjas for lunch. This is one of the most beautiful rivers of the entire week passing through two amazing gorges. After lunch we can choose to run the Azul again or try our hand at a section of the Futaleufú River. Afterwards we will return to camp for hot saunas and dinner in camp.</p>
                                <p><strong>Day 7:</strong>
                                        This morning we can try our hand on the high-volume Macal section of the Futaleufú River or choose to challenge another section! This is our grand finale of rivers for the week. Tonight we say farewell with a traditional “Asado” (barbecue) on the beach and toast each other and the week’s paddling adventures.</p>
                                <p><strong>Day 8:</strong>
                                        We breakfast at Campo before the drive back to San Carlos de Bariloche and lunch en route. We will drop you at your hotel in Bariloche. Breakfast and lunch are included in your trip but dinner is on your own. Alternatively, we can drop you off in Esquel or El Bolsón if you wish.</p>
                                <p><strong>Please note that this is a sample itinerary only. Your trip will most likely be customized to your group and paddling conditions. Accommodation and river choices can vary from trip to trip!</strong></p>
                              </div>
                            </div>
                          </div>
                          <div class="panel">
                            <div class="panel-heading">
                            <h4 class="panel-title">
                              <a href="#acomodations" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> Accommodations</a>
                              </h4>
                            </div>
                            <div id="acomodations" class="panel-collapse collapse">
                            <div class="panel-body cart2">
                                    <p> <strong>Play hard, rest easy.</strong>
                                        Most Paddle Patagonia trips stay in comfortable and clean lodges and cabins where you do not need to bring any bedding. There is no camping required although it could be an option if requested by your group. Some locations where you will stay have saunas and/or hot tubs which are a real luxury after a long day of paddling! Ask us about the accommodation options for your trip!</p>
                              </div>
                            </div>
                          </div>
                          <div class="panel">
                            <div class="panel-heading">
                            <h4 class="panel-title">
                              <a href="#Logistics" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> Logistics</a>
                              </h4>
                            </div>
                            <div id="Logistics" class="panel-collapse collapse">
                            <div class="panel-body cart2">
                                    <p><strong>Weather</strong>
                                        Paddle Patagonia’s trips travel through several different mountain terrains and at anytime the weather can change often and quickly. In general, the early season may see cooler mornings/evenings with hot days (20+ C°). As we get closer to Jan-Feb, the weather stabilizes into warmer summerlike conditions (25+ C°). The sun in Patagonia can be quite intense and a good sunscreen is advised.
                                    </p>
                                    <p><strong>Water</strong>
                                        All of the rivers we paddle are crystal clear and clean. Water temperatures will range from 8-15C°. See packing list for details on what to bring.


                                    </p>
                                    <h3>Packing List</h3>
                                    <h4>Required personal paddling equipment:</h4>
                                    <ul>
                                             <li><i class="fas fa-caret-right"></i>  Helmet</li>
                                            <li><i class="fas fa-caret-right"></i>  Paddle</li>
                                            <li><i  class="fas fa-caret-right"></i> Lifejacket & whistle</li>
                                            <li><i class="fas fa-caret-right"></i>  Good footwear for paddling (we recommend something with thick soles that is supportive enough on rough terrain, a pair of sneakers will work)</li>
                                            <li><i class="fas fa-caret-right"></i>  Sprayskirt (for kayakers)</li>
                                            <li><i class="fas fa-caret-right"></i>  Water bottle</li>
                                     </ul>
                                     <p>Some items may be available for rent – please inquire.</p>
                                      <h4>Recommended paddling layers:</h4>
                                      <ul>
                                          <li><i class="fas fa-caret-right"></i> Wool, polypro or neoprene tops (at least 2) </li>
                                          <li><i class="fas fa-caret-right"></i> Wool, polypro or neoprene bottoms (at least 2) </li>
                                          <li><i class="fas fa-caret-right"></i> Shorts </li>
                                          <li><i class="fas fa-caret-right"></i> Canoers: Drysuit or wetsuit recommended </li>
                                          <li><i class="fas fa-caret-right"></i> Kayakers: Longsleeve drytop recommended </li>
                                          <li><i class="fas fa-caret-right"></i> Wool or neoprene socks </li>
                                      </ul>
                                      <h4>Suggested personal gear:</h4>
                                      <ul>
                                          <li><i class="fas fa-caret-right"></i> Spring/summer clothing </li>
                                          <li><i class="fas fa-caret-right"></i> Long pants </li>
                                          <li><i class="fas fa-caret-right"></i> Warm sweater or fleece </li>
                                          <li><i class="fas fa-caret-right"></i> Rain coat </li>
                                          <li><i class="fas fa-caret-right"></i> Baseball or Sun hat </li>
                                          <li><i class="fas fa-caret-right"></i> Sunglasses with strap </li>
                                          <li><i class="fas fa-caret-right"></i> Sunscreen </li>
                                          <li><i class="fas fa-caret-right"></i> Wool or fleece hat </li>
                                          <li><i class="fas fa-caret-right"></i> Shoes for hiking (either running shoes or hiking boots will do) </li>
                                          <li><i class="fas fa-caret-right"></i> Sandals or slip on shoes to wear indoors/off the river </li>
                                          <li><i class="fas fa-caret-right"></i> Personal toiletries </li>
                                          <li><i class="fas fa-caret-right"></i> Head lamp </li>
                                          <li><i class="fas fa-caret-right"></i> Light pack towel or sarong for riverside changing </li>
                                          <li><i class="fas fa-caret-right"></i> Ear plugs & Nose plugs </li>
                                          <li><i class="fas fa-caret-right"></i> Kayak sponge </li>
                                          <li><i class="fas fa-caret-right"></i> Throw bag (all of our guides always carry one but it is good practice to carry your own) </li>

                                      </ul>
                                      <p><strong>It is recommended to make photocopies of all your important documents prior to departure. Keep a separate copy in your checked luggage and carry the originals with you</strong></p>
                                      <h4>Documents:</h4>
                                          <ul>
                                              <li><i class="fas fa-caret-right"></i> Passport</li>
                                              <li><i class="fas fa-caret-right"></i> Travel & Medical Insurance</li>
                                              <li><i class="fas fa-caret-right"></i> Emergency Contact information</li>
                                          </ul>
                                    <h3>Getting Here:</h3>
                                    <h4>Option 1:</h4>
                                    <p><strong>Fly to Buenos Aires in Argentina, connect to San Carlos de Bariloche.</strong></p>
                                    <p>Most international flights fly into Ministro Pistarini International Airport in Buenos Aires, Argentina (also known as Ezeiza), the airport code is EZE. If flying into Bariloche you will need to get to the domestic airport called Jorge Newbery Airport (also known as Aeroparque), the airport code is AEP. From there you can take a connecting flight to San Carlos de Bariloche (BRC).</p>
                                    <p>How to get from Ezeiza (EZE) to Aeroparque (AEP):</p>
                                    <ul>
                                        <li><i class="fas fa-caret-right"></i> <strong>Transport Bus Service:</strong>There is an official bus line called “Manuel Tienda Leon” that transports travelers to other locations in Buenos Aires. You purchase a ticket from their booth after clearing customs (they have one inside and one outside). The cost is approximately $16 USD per person and they have departures approximately every 20 minutes. (www.tiendaleon.com.ar).</li>
                                        <li><i class="fas fa-caret-right"></i> <strong>By Taxi:</strong>This will cost approximately $50 USD per taxi. Upon exiting customs you will see several official taxi companies. You purchase a standard fare to Aeroparque from one of those stands by cash or credit card, they give you a ticket and you then take that to the official taxi line. This option is faster and one taxi will take up to 4 people. However if you have extra or oversized bags you may want to take the bus service.</li>
                                    </ul>
                                    <h4>Option 2:</h4>
                                    <p><strong>Fly to Santiago in Chile, connect to San Carlos de Bariloche, Argentina.</strong></p>
                                    <p>You may also choose to fly into Santiago de Chile. The international airport is called Arturo Merino Benitez and the airport code is SCL. From there you can get a connecting flight to San Carlos de Bariloche (BRC), usually via Aeroparque Buenos Aires (AEP).</p>
                                    <h3>Arriving into Argentina</h3>
                                    <h4>THE RECIPROCITY FEE:</h4>
                                    <p><strong>Some nationalities now need to purchase a “reciprocity entry fee” online ahead of time, namely Canadian, American and Australians.</strong>Most European countries do not have to pay but for the others the cost could be between $70 and $130 USD with varying validity. You must purchase this by credit card online in advance and may be able to choose a single entry or a multiple entry visa. You DO NOT need a multiple entry visa for crossing in and out of Argentina and Chile by land. Only purchase multiple entry if you plan on flying into the country again in the next 5 years. Please check with your local embassy to confirm the regulations for your country of origin. This was accurate at the time of writing but please double check as rules may have changed!</p>
                                    <p>Here is the website link for purchasing your reciprocity fee: <a href="http://www.migraciones.gov.ar/accesibleingles/">http://www.migraciones.gov.ar/accesibleingles/</a></p>
                                    <p><strong>Make sure you print out the receipt for your reciprocity fee and carry it with your passport.</strong></p>
                              </div>
                            </div>
                        </div>


                        </div>
                      </div>
                </div>
            </div>
        </section>
   <!-- EndAcordeon-->


</div>
</section>


<div id="rescue" class="jumbotron paral paralsec1 rel">

    <h2 class="display-3">WHITEWATER RESCUE</h2>

</div>

<section class="one">
    <div class="container">
        <div class="row">
        <div class="col-md-12 textwhite">
                <h2>Boreal River Rescue</h2>

                <p>Paddle Patagonia works under the certification program of Boreal River Rescue and delivers the best whitewater and swiftwater rescue training courses for river guides, paddling instructors, boaters, field workers, and rescue professionals.</p>
                 <p>Our programs are intensive, hands-on, and well run. The curriculum is constantly updated to stay relevant and useful. Courses include a combination of skill practice, rescue simulations, discussions, and practical reviews.</p>

                 <p>Boreal River Rescue instructors have been setting the standard in whitewater and swiftwater rescue training internationally for over a decade. We have experience performing rescues and teaching whitewater skills in a range of river environments, from instructing paddlers on wilderness river trips, to teaching firefighters in urban flood-canals, to training raft guides on jungle rivers. Our instructors are highly experienced, professional, and fun.</p>

                <p>We are happy to customize a trip to suit your group’s particular paddling interests and abilities and we also have many non-paddling activities to offer if you want to take a break from the rivers for a day or two.  On most rivers you can choose to canoe, kayak, inflatable kayak or raft.    Contact us to see your options and build the perfect paddling itinerary for your group!</p>
                <p><strong>Organize a course for your company, club, group, or school.</strong></p>
                <p>Hold a Boreal River Rescue course at your location. We provide the instructors, equipment, and a great course. You organize the group. This can be for your own staff team or group members or open to the public. We’ll work with you to make sure that the course runs smoothly and we’ll help you spread the word. Email: <a href="info@paddlepatagonia.com">info@paddlepatagonia.com</a> to get started.

                </p>
        </div>
        </div>
    </div>

    </section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="no_padding vide_block d-flex bg_img justify_content_center align_items_center bg_overlay1">

                    <div class="vide_text_icon text-center">
                        <a href="https://www.youtube.com/watch?v=yJHxXJEO-YQ" class="popup_video"><img src="assets/images/play-button.png" alt=""></a>
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>
    <section class="two">
        <div class="container">
            <div class="row">
                <div class="col-md-12 textwhite">
                    <h3>Courses Options:</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="accordion" class="panel-group">
                      <div class="panel">
                        <div class="panel-heading">
                        <h4 class="panel-title">
                          <a href="#panelBodyOne" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"> Whitewater Rescue (WWR) – two days (levels I & II)</a>
                          </h4>
                        </div>
                        <div id="panelBodyOne" class="panel-collapse collapse in" >
                        <div class="panel-body">
                            <p>The Whitewater Rescue (WWR) course is a fun and full two days of learning and practice in and around the river. Get a foundation of rescue skills and knowledge so that you can work and play safely around whitewater.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel">
                        <div class="panel-heading">
                        <h4 class="panel-title">
                          <a href="#panelBodyTwo" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> Whitewater Rescue Technician (WRT) – four days (levels I, II & III)</a>
                          </h4>
                        </div>
                        <div id="panelBodyTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>The international standard for river guides, whitewater instructors, advanced boaters, and rescue technicians. On this intensive and hands-on course you will practice and perfect rescue skills in a variety of whitewater environments.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel">
                        <div class="panel-heading">
                        <h4 class="panel-title">
                          <a href="#panelBodyThree" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> Whitewater Rescue Technician – Bridge – two days (level III)</a>
                          </h4>
                        </div>
                        <div id="panelBodyThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>This is for graduates of Whitewater Rescue (WWR) courses who wish to upgrade their skills and certification to the Whitewater Rescue Technician (WRT) level. To enroll you must have a current WWR certification or equivalent whitewater or swiftwater rescue certification from another provider such as the American Canoe Association, Rescue Canada, Rescue for River Runners, or Rescue 3 International.</p>
                          </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                        <h4 class="panel-title">
                          <a href="#panelBodyfor" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> Whitewater Rescue Technician Recertification – two days (level III)</a>
                          </h4>
                        </div>
                        <div id="panelBodyfor" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>The two-day WRT-Recert is for those with current WRT certification from Boreal River Rescue or an equivalent certification from another provider such as R3-pro from Rescue for River Runners or Swiftwater Rescue Technician – Level 1(SRT-1) from Rescue 3 International, Rescue Canada, or the American Canoe Association.</p>
                          </div>
                        </div>
                    </div>

                    </div>
                  </div>
            </div>
        </div>
    </section>

<!--Tabs-->
<section class="two">
    <div class="container">
        <div class="row">
            <div class="col-md-12 textwhite">
                <h2>Whitewater Rescue WWR – 2 days</h2>
            </div>

        </div>
        <div class="row">

            <div class="col-md-12 textwhite">
                <p>The Whitewater Rescue (WWR) course covers the basics of safety and rescue for those who paddle, work, and travel in rivers. This is a full, thorough, and fun two days of hands-on training.</p>
                <p>Increase your understanding of the river environment in order to identify and avoid hazards and work with the water to perform efficient and effective rescues. Use swimming, wading, and throw ropes to move around the river and to rescue yourself and others. Get a basis in knots, anchors, and rope systems for use in equipment recovery and transportation systems. This course provides a foundation of rescue skills and knowledge so that you can work and play safely around whitewater.</p>
                <p>Graduates of White Water Rescue (WWR) can upgrade to Whitewater Rescue Technician (WRT) by taking the two-day WRT-Bridge course within three years of their original certification.</p>
            </div>
        </div>
    </div>
</section>
<section class="two">
    <div class="container">
        <div class="row">
            <div class="col-md-12 textwhite">
                <h2>Whitewater Rescue Technician WRT – 4 days</h2>
            </div>

        </div>
        <div class="row">

            <div class="col-md-12 textwhite">
                <p>Whitewater Rescue Technician (WRT) is the international standard for river guides, whitewater instructors, independent boaters, and rescue technicians. The course takes place over four full days on and around the river.</p>
                <p>Increase your understanding of the river environment in order to identify and avoid hazards and work with the water to perform efficient and effective rescues. After learning techniques for swimming, wading, throwing ropes, and rescuing people in relatively easy rapids, you will refine and gain expertise with these skills in a variety of bigger rapids and more complex situations.</p>
                <p>The second half of the course involves simulations where you will practice your whitewater rescue skills along with leadership, communication, and problem solving. The final day includes a practical assessment.</p>
            </div>
        </div>
    </div>
</section>
<section class="two">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div id="accordion" class="panel-group">
                  <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#overview" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"> Overview</a>
                      </h4>
                    </div>
                    <div id="overview" class="panel-collapse collapse in" >
                    <div class="panel-body cart2">
                        <h5>Cost: $?? USD</h5>
                        <h5>Included:</h5>
                        <ul>
                            <li><i class="fas fa-caret-right"></i> Whitewater Rescue  (WWR) certification</li>
                            <li><i class="fas fa-caret-right"></i> Waterproof fold-out guide</li>
                        </ul>
                        <h5>Not included:</h5>
                        <ul>
                            <li><i class="fas fa-caret-right"></i> Transport to the course location</li>
                            <li><i class="fas fa-caret-right"></i> Medical & travel insurance (required)
                            </li>
                            <li><i class="fas fa-caret-right"></i> Personal protective gear (helmet, PFD with rescue harness)</li>
                            <li><i class="fas fa-caret-right"></i> Appropriate footwear and clothing</li>
                        </ul>
                        <h5>Prerequisites:</h5>
                        <ul>
                            <li><i class="fas fa-caret-right"></i> Students must be 16 years of age to participate in this course. 16 and 17 year olds must have written proof of parental consent.</li>
                            <li><i class="fas fa-caret-right"></i> Ability to swim</li>

                        </ul>


                      </div>
                    </div>
                  </div>
                  <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#Topics" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> Topics Coreved</a>
                      </h4>
                    </div>
                    <div id="Topics" class="panel-collapse collapse">
                    <div class="panel-body cart2">
                        <ul>
                            <li><i class="fas fa-caret-right"></i> General concepts of river rescue</li>
                            <li><i class="fas fa-caret-right"></i> Personal and group equipment</li>
                            <li><i class="fas fa-caret-right"></i> River safety and scene management</li>
                            <li><i class="fas fa-caret-right"></i> River signals and communications</li>
                            <li><i class="fas fa-caret-right"></i> River features and hydrology</li>
                            <li><i class="fas fa-caret-right"></i> Swimming in moving water: self rescue and whitewater maneuvers</li>
                            <li><i class="fas fa-caret-right"></i> Throw ropes</li>
                            <li><i class="fas fa-caret-right"></i> Contact rescues</li>
                            <li><i class="fas fa-caret-right"></i> Introduction to quick release rescue harness and ‘live bait’ rescue</li>
                            <li><i class="fas fa-caret-right"></i> Rescue of panicked subjects</li>
                            <li><i class="fas fa-caret-right"></i> Shallow water crossings</li>
                            <li><i class="fas fa-caret-right"></i> Foot entrapment</li>
                            <li><i class="fas fa-caret-right"></i> Knots, anchors, and basic tensioning systems</li>
                            <li><i class="fas fa-caret-right"></i> Boat pins and wraps</li>
                            <li><i class="fas fa-caret-right"></i> ‘Strainers’</li>
                            <li><i class="fas fa-caret-right"></i> Rescue scenarios</li>

                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#logistics" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> Logistics</a>
                      </h4>
                    </div>
                    <div id="logistics" class="panel-collapse collapse">
                    <div class="panel-body cart2">
                        <h5>Accommodations and Meals</h5>
                        <p>Different options are available depending on where and when the course is taking place. Inquire with the office for details.</p>
                        <h5>Weather</h5>
                        <p>Paddle Patagonia courses and trips are located in different mountain terrains and at anytime the weather can change often and quickly. In general, the early season may see cooler mornings/evenings with hot days (20+ C°). As we get closer to Jan-Feb, the weather stabilizes into warmer summer-like conditions (25+ C°). The sun in Patagonia can be quite intense and a good sunscreen is advised.</p>
                        <h5>Water</h5>
                      <p>All of the rivers we run courses on are crystal clear and clean. Water temperatures will range from 10-20C°. See packing list for details on what to bring.</p>
                      </div>

                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#pack" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> Packing List</a>
                      </h4>
                    </div>
                    <div id="pack" class="panel-collapse collapse">
                    <div class="panel-body cart2">
                        <p>This is a general list of what to bring for Whitewater Rescue (WWR) and Whitewater Rescue Technician (WRT) courses. Please refer to your ‘student information package’ for a complete list for your specific course and to see if rental items are available.</p>
                        <h5>For the river:</h5>
                        <ul>
                            <li><i class="fas fa-caret-right"></i> A whitewater helmet that fits properly</li>
                            <li><i class="fas fa-caret-right"></i> <strong>Personal floatation device (PFD)</strong>
                                WRT students will need a PFD with ‘quick-release’ rescue harness, it is not mandatory for WWR students.  The PFD needs to be in good condition and fit properly.</li>
                            <li><i class="fas fa-caret-right"></i> <strong>River shoes</strong>
                                The ideal river shoe has full toe covering and is supportive but also lightweight and draining with a grippy sole. This could be a model made specifically for the river or a lightweight running shoe, trail-running shoe, or sneaker. Neoprene shoes can work if they have enough support and traction but neoprene socks or booties are not adequate.</li>
                            <li><i class="fas fa-caret-right"></i> <strong>Wetsuit or drysuit</strong>
                                A wetsuit or drysuit is necessary for spring and fall courses and always necessary on glacier-fed rivers.</li>
                            <li><i class="fas fa-caret-right"></i> <strong>Base layers, insulation, and socks.</strong>
                                Make sure to have several of layers or socks, pants, and tops made of wool, fleece, polyester, or another synthetic material to wear in addition to your wetsuit or drysuit (or to be worn on their own on sunny days and in warm water).</li>
                            <li><i class="fas fa-caret-right"></i> Surf shorts</li>
                            <li><i class="fas fa-caret-right"></i> Rash guard</li>
                            <li><i class="fas fa-caret-right"></i> <strong>Snacks and water bottle or thermos</strong>Keeping yourself fueled and hydrated is one of the keys to staying warm and happy. High-calorie snacks like chocolate bars and cereal bars are great to keep in your pfd.</li>
                            <li><i class="fas fa-caret-right"></i> Sunscreen, lip protection</li>
                            <li><i class="fas fa-caret-right"></i> Neoprene gloves (optional)</li>
                            <li><i class="fas fa-caret-right"></i> Neoprene head warmer (optional)</li>
                            <li><i class="fas fa-caret-right"></i> Camera (optional)</li>
                            <li><i class="fas fa-caret-right"></i> Sunglasses or prescription glasses with strap (optional)</li>
                        </ul>
                        <h5>For on Land:</h5>
                        <ul>
                            <li><i class="fas fa-caret-right"></i> Comfortable clothing</li>
                            <li><i class="fas fa-caret-right"></i> Comfortable shoes</li>
                            <li><i class="fas fa-caret-right"></i> Warn hat, gloves, or mitts for early and late season courses</li>
                            <li><i class="fas fa-caret-right"></i> Sun hat, visor, or baseball cap</li>
                            <li><i class="fas fa-caret-right"></i> Raingear</li>
                            <li><i class="fas fa-caret-right"></i> Gear bag or container to store your equipment for transport</li>
                            <li><i class="fas fa-caret-right"></i> Day pack or drybag to store your belongings during class</li>
                            <li><i class="fas fa-caret-right"></i> Pens and notepad</li>
                        </ul>
                        <h5>Consider the following items <span class="texsmall">(depending on the location and accommodation options for your course).</span></h5>
                        <ul>
                            <li><i class="fas fa-caret-right"></i> Meals and cooking equipment</li>
                            <li><i class="fas fa-caret-right"></i> Camping equipment or bedding</li>
                            <li><i class="fas fa-caret-right"></i> Towel</li>
                            <li><i class="fas fa-caret-right"></i> Personal hygiene products and medications</li>
                            <li><i class="fas fa-caret-right"></i> Indoor clothing and shoes</li>
                        </ul>

                      </div>
                    </div>
                </div>
                <div class="panel">
                        <div class="panel-heading">
                        <h4 class="panel-title">
                          <a href="#faqs" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> FAQs</a>
                          </h4>
                        </div>
                        <div id="faqs" class="panel-collapse collapse">
                        <div class="panel-body cart2">
                            <h5>What is the daily schedule like on a Boreal River Rescue course?</h5>
                                <p>Typically class starts at 8:30am and goes to 6:30pm. Evenings are reserved for homework assignments and practicing on-land skills with other students.  There may also be an evening session that will be scheduled by the instructor on the first day.</p>
                            <h5>Are there any prerequisites?</h5>
                                <p>For all courses students must be 16 years of age to participate.  16 and 17 year olds must have written proof of parental consent.</p>
                            <h5>Whitewater Rescue (WWR) prerequisites:</h5>
                            <ul>
                                <li><i class="fas fa-caret-right"></i> No prerequisites</li>
                            </ul>
                            <h5>Whitewater Rescue Technician (WRT) prerequisites</h5>
                            <ul>
                                <li><i class="fas fa-caret-right"></i> Swimming ability</li>
                            </ul>
                            <h5>Whitewater Rescue Technician – Bridge (WRT-Bridge) prerequisites:</h5>
                            <ul>
                                <li><i class="fas fa-caret-right"></i> Swimming Ability</li>
                                <li><i class="fas fa-caret-right"></i> Completion of pre-course practical and written assignment</li>
                                <li><i class="fas fa-caret-right"></i> Current Whitewater Rescue (WWR) certification from Boreal River Rescue or current equivalent certification such as Rescue for River Runners, River Rescue Certification, or Swiftwater Rescue from another approved provider such as American Canoe Association, Rescue Canada, or Rescue 3 International.  If you are currently certified through another provider, please contact our office for approval.</li>
                            </ul>
                            <h5>Whitewater Rescue Technician – Recertification (WRT-Recert) prerequisites:</h5>
                            <ul>
                                <li><i class="fas fa-caret-right"></i> Swimming Ability</li>
                                <li><i class="fas fa-caret-right"></i> Completion of pre-course practical and written assignment</li>
                                <li><i class="fas fa-caret-right"></i> Current Whitewater Rescue Technician (WRT) certification from Boreal River Rescue or current equivalent certification from another approved provider such as Swiftwater Rescue Technician Advanced (SRT-A), Swiftwater Rescue Technician – Level 1 (SRT-1), or Whitewater Rescue Technician – Level 1 (WRT-1)  from the American Canoe Association, Rescue Canada, or Rescue 3 International.  If you are currently certified through another provider, please contact our office for approval.</li>
                            </ul>
                            <h5>Is attendance mandatory?</h5>
                            <p>Attendance for 100% of class time is mandatory.  If you cannot make a session you must get permission from the instructor in advance.  Students who miss a section of the course without pre-approval may be asked to leave the course for the sake and safety of the other students.</p>
                            <h5>How are students assessed and evaluated for certification?</h5>
                            <h5>Whitewater Rescue (WWR) evaluation criteria:</h5>
                            <ul>
                                <li><i class="fas fa-caret-right"></i> Practical assessment is ongoing within a supportive, professional, and fun learning environment.</li>
                                <li><i class="fas fa-caret-right"></i> To achieve the WWR certification students need to attend all sessions and participate throughout the course</li>
                                <li><i class="fas fa-caret-right"></i> Demonstrate an understanding of river safety and river rescue skills</li>
                            </ul>
                            <h5>Whitewater Rescue Technician (WRT, WRT-Bridge, WRT-Recert) evaluation criteria:</h5>
                            <ul>
                                <li><i class="fas fa-caret-right"></i> One of the most valuable aspects of Boreal River Rescue – Whitewater Rescue Technician (WRT) certification is that it includes a practical evaluation.  This proves that an individual holding the certification has demonstrated competency with their river rescue skills and not simply attended a workshop. The assessment takes place on the river in a supportive, fun, and professional environment with the goals of evaluation, review, and continued learning. Students will get multiple chances to demonstrate competency with their skills. To achieve the WRT certification students need to:</li>
                                <li><i class="fas fa-caret-right"></i> Attend all sessions and participate throughout the course</li>
                                <li><i class="fas fa-caret-right"></i> Demonstrate an understanding of river safety and river rescue skills</li>
                                <li><i class="fas fa-caret-right"></i> Pass the practical evaluation on the last day.

                                </li>
                            </ul>
                            <h5>How long is certification valid for?</h5>
                            <p>Your certification is valid for three years from the date of issue.</p>
                            <h5>How do I get re-certified in Whitewater Rescue (WWR)?</h5>
                            <p>You should consider upgrading from Whitewater Rescue (WWR) to Whitewater Rescue Technician (WRT) by taking the two-day Whitewater Rescue Technician – Bridge (WRT-Bridge) course. Or you can re-certify your Whitewater Rescue (WWR) by re-taking the course.
                            </p>
                            <h5>How do I get re-certified as a Whitewater Rescue Technician (WRT)?</h5>
                            <p>You can re-certify your Whitewater Rescue Technician (WRT) certification by taking the two-dayWhitewater Rescue Technician Recertification (WRT-Recert) course within three years of your lastcertification. You must complete a pre-course written and practical assignment by the start of thecourse.  Please bring your current certification card with you to your course. You can also re-takethe four-day course if you would like even more practice.</p>
                            <h5>I have current certification but I’m not sure if the two-day Recert/Bridge option is right forme or if I should take the four-day WRT course?</h5>
                            <p>In order to successfully re-certify or upgrade to your technician-level certification in two-days it is important that you arrive for your course ready to use your skills and knowledge from yourprevious training courses and experiences.  The two-day option is great if you have been practicingmost of your skills regularly and spending time in and around rivers during the three years sinceyour last course. We recommend taking the complete four-day WRT course if you would like moresupervised practice time, if you haven’t spent very much time around rivers since your last course,or if you aren’t able to perform the skills outlined in the pre-course assignment.</p>
                            <h5>Can I take the WRT-Bridge or WRT-Recert course if my current certification is through another provider and not Boreal River Rescue?</h5>
                            <p>Yes.  Please contact the office for approval.</p>









                          </div>

                        </div>
                    </div>

                </div>
              </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
	<script src="/assets/js/tab.js"></script>

@endsection
