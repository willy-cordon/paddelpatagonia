@extends('layouts.master')

@section('content')
@section('titulo')
    Trips All
@endsection
@include('componets.headPage')





<div class="type_page_wrapper section_padding_60_60">

		<!--Start container-->
		<div class="container">


    <div class="row">
            @foreach ($trips as $trip)


            <div class="item_service_card col-md-6 col-lg-4">
                    <div class="image_service">
                        <img src="{{ $trip->getFirstMediaUrl('gallery', 'thumb') }}" alt="{{ $trip->title }}">
                    </div>
                    <div class="describe_service">
                        <h4 class="name_service">
                           {{$trip->title}}
                        </h4>
                    <h3 class="text-center">{{$trip->formated_price}}</h3>
                    </div>
                    <div class="overlay">
                        <h4 class="name_service">
                            <a href="{{ route('singleTrip', ['slug' => $trip->slug]) }}">{{$trip->title}}</a>
                        </h4>
                    <h6>{{$trip->formated_price}}</h6>

                        <p>{!! $trip->body !!}</p>

                        <div class="boton2">
                                <a  href="{{ route('singleTrip', ['slug' => $trip->slug]) }}">See More</a>
                            </div>


                    </div>
                </div>


            @endforeach
    </div>
			<nav class="page_pagination_wrapper">
                        @if ($trips->count())
                        {{$trips->links()}}
                       @endif
			</nav>

		</div>

	</div>





@endsection




