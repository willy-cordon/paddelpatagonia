@extends('layouts.master')

@section('title')
    FaQs - Paddel Patagonia
@endsection
@section('titulo')
    FAQS
@endsection

@section('content')
@include('componets.headPage')

<section class="two">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div id="accordion" class="panel-group">
                  <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#Wherepatagonia" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion">Where is Patagonia?</a>
                      </h4>
                    </div>
                    <div id="Wherepatagonia" class="panel-collapse collapse in" >
                    <div class="panel-body">
                        <p>Patagonia is a region located at the southern end of South America and is a territory shared by both Argentina and Chile. It includes the Argentine provinces of Neuquen, Rio Negro, Chubut and Santa Cruz as well as Tierra del Fuego. The Chilean portion includes the southern provinces of Valdivia, Llanquihue, Aisen and Magallanes as well as the Chilean portion of Tierra del Fuego. In total the region is over 1 million square kilometers.</p>
                        <p>Patagonia includes the Andean mountain range as well as huge plateaus that extend east of the Andes in a series of broad, flat steps extending to the Atlantic coast. The coastline is cliffed along its entire length and the landscape is cut by many rivers some of which have glacial origin in the Andes creating broad valleys and steep-walled canyons.</p>
                      </div>
                    </div>
                  </div>
                  <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#panel2" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">Are Argentina and Chile safe travel destinations?</a>
                      </h4>
                    </div>
                    <div id="panel2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Argentina and Chile are rated the safest countries in Latin America for tourists. Of course when traveling anywhere you will want to take precautions against petty crime and keep an eye on your belongings, especially in big cities and bus stations.</p>
                      </div>
                    </div>
                  </div>
                  <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#panel3" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> Do I need to speak Spanish to travel in Argentina or Chile?</a>
                      </h4>
                    </div>
                    <div id="panel3" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Learning a few basics will help you communicate better but non-Spanish speakers can get by just fine and many people speak English. However be warned if you have learned Spanish in another country that Argentineans speak quickly and they use a slightly different dialect called “Castellano.” We are happy to give you a few lessons once you arrive!</p>
                      </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#panel4" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">Where does my Paddle Patagonia trip start?</a>
                      </h4>
                    </div>
                    <div id="panel4" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Most of Paddle Patagonia’s trips begin and end in El Bolson or San Carlos de Bariloche in Argentina. We can make arrangements to meet you and drop you off at the airport, bus station or your hotel.</p>
                      </div>
                    </div>
                </div>
                <!--Panels-->
                <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#panel5" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> Which city should I fly into?</a>
                      </h4>
                    </div>
                    <div id="panel5" class="panel-collapse collapse in" >
                    <div class="panel-body">
                        <p>Most people will fly into Buenos Aires, Argentina where you can then connect to Bariloche (BRC). Please note that there are 2 airports in Buenos Aires and you will most likely have to transfer between airports to connect to Bariloche. You may also look at flying into Santiago, Chile (SCL) and connect to Bariloche from there. See the “getting here” section of your trip itinerary for more details.</p>
                      </div>
                    </div>
                  </div>
                   <!--Panels-->
                <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#panel6" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">Do I need a visa for Argentina or Chile?</a>
                      </h4>
                    </div>
                    <div id="panel6" class="panel-collapse collapse in" >
                    <div class="panel-body">
                        <p>Some nationalities now need to purchase a “reciprocity entry fee” online ahead of time, namely Canadian, American and Australians. Most European countries do not have to pay but for the others the cost could be between $70 and $130 USD with varying validity. <strong>You must purchase this by credit card online in advance</strong> and may be able to choose a single entry or a multiple entry visa. You DO NOT need a multiple entry visa for crossing in and out of Argentina and Chile by land. Please check with your local embassy to confirm the regulations for your country of origin.</p>
                        <p>Here is the website link for purchasing your reciprocity fee: http://www.migraciones.gov.ar/accesibleingles/</p>
                        <p>Make sure you print out the receipt for your reciprocity fee and carry it with your passport.</p>
                      </div>
                    </div>
                  </div>
                   <!--Panels-->
                <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#panel7" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">What type of currency should I travel with?</a>
                      </h4>
                    </div>
                    <div id="panel7" class="panel-collapse collapse in" >
                    <div class="panel-body">
                        <p>Not an easy answer these days!  The Argentinean Peso is the official currency of Argentina but due to economic issues in the country the peso is not the most stable currency to purchase in advance.  We suggest traveling with USD or Euros and exchanging as you need.  At the time of writing the country was operating on a dual system with the official exchange rate and the “blue rate” which is more or less a highly accepted black market rate for the USD. If you use your foreign credit card for purchases you will be paying the official exchange rate as determined by your home bank and potentially additional fees for not paying in cash.  If you withdraw pesos at an ATM, you will also be exchanging at the less favorable official rate.  Most businesses do not accept USD directly and you will need to find a “casa de cambio” to exchange your USD or Euros into pesos upon arrival.  Your hotel in Buenos Aires or Bariloche can most likely suggest the best place to exchange.</p>
                      </div>
                    </div>
                  </div>
                   <!--Panels-->
                <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#panel8" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> Can I take the bus to Bariloche?</a>
                      </h4>
                    </div>
                    <div id="panel8" class="panel-collapse collapse in" >
                    <div class="panel-body">
                        <p>
                            Yes, absolutely! If you have some extra time in your travel plans, then taking the bus to Bariloche can be a good alternative. You can take a bus from Buenos Aires to Bariloche for $100-$200 USD each way. The buses in Argentina and Chile are modern, clean and comfortable (much better than greyhound in North America!) The trip is long (20+ hours) but very comfortable and a great way to see the magnificent landscapes of this country from your window! There are 3 kinds of seats to choose from that vary from just reclining to fully horizontal beds. The buses all have washrooms on board as well as attendants that serve complimentary beverages, snacks and hot meals. Contact us to find out more about which bus lines we recommend and how to book your tickets.</p>
                      </div>
                    </div>
                  </div>
                   <!--Panels-->
                <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#panel9" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">What kind of insurance do I need?</a>
                      </h4>
                    </div>
                    <div id="panel9" class="panel-collapse collapse in" >
                    <div class="panel-body">
                        <p>We recommend having appropriate medical and travel insurance for your trip to Patagonia. We also strongly recommend obtaining trip cancellation insurance that will protect your trip investment if you have to cancel for personal reasons. If you have insurance coverage from a credit card or an existing plan, please make sure that it will cover you for whitewater activities and remote wilderness and adventure travel. Please review our cancellation policy in regards to trip payments.</p>
                      </div>
                    </div>
                  </div>
                   <!--Panels-->
                <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#panel10" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">What type of whitewater skills do I need?</a>
                      </h4>
                    </div>
                    <div id="panel10" class="panel-collapse collapse in" >
                    <div class="panel-body">
                        <p>
                            For whitewater canoeists and kayakers you should have experience paddling a variety of class III rapids for the class II-III trip and a variety of class IV rapids for the class III-IV trip. For kayakers, although we don’t expect a 100% bomb-proof roll, a solid roll in whitewater is recommended. Spending some time on your home river or local pool working on getting your roll perfected before you arrive in Patagonia can go a long way. If you are joining us on a non-paddler itinerary you do not need any whitewater experience as you will be joined by a guide in your raft or led by a guide in your inflatable kayak at all times.</p>
                      </div>
                    </div>
                  </div>
                   <!--Panels-->
                <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#panel11" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> Who will my guides be?</a>
                      </h4>
                    </div>
                    <div id="panel11" class="panel-collapse collapse in" >
                    <div class="panel-body">
                        <p>All of Paddle Patagonia’s guides are English speaking whitewater professionals with years of experience guiding and instructing in Patagonia as well as other regions of the world. They are also fun people who are excited about sharing their passion for the region and the rivers with you! Visit the “About Us” page on our website for more information about our staff.</p>
                      </div>
                    </div>
                  </div>
                   <!--Panels-->
                <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#panel12" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> What is the water like on the rivers we paddle?</a>
                      </h4>
                    </div>
                    <div id="panel12" class="panel-collapse collapse in" >
                    <div class="panel-body">
                        <p>All of the rivers we paddle are crystal clear and clean. Some rivers are glacier-fed and water temperatures will range from 10-18C°. See packing list for details on what to bring.</p>
                      </div>
                    </div>
                  </div>
                   <!--Panels-->
                <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#panel13" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">What is the food like?</a>
                      </h4>
                    </div>
                    <div id="panel13" class="panel-collapse collapse in" >
                    <div class="panel-body">
                        <p>Argentina is famous for its red wine and beef barbecues, which are certainly a highlight for many but there is so much more! Unlike other Latin American countries, you will notice a very European influence in the meals that include fresh salads, pastas, grilled fish, baked goods and more. All of Paddle Patagonia meals are freshly made from local ingredients whenever possible and the food is not generally spicy or greasy. Being a vegetarian is no problem and we will do our best to cater to any other special dietary requests. Tap water is potable and there are no issues with drinking water throughout the country.</p>
                      </div>
                    </div>
                  </div>
                   <!--Panels-->
                <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#panel14" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> What kind of accommodations will I stay in?</a>
                      </h4>
                    </div>
                    <div id="panel14" class="panel-collapse collapse in" >
                    <div class="panel-body">
                        <p>Paddle Patagonia’s trips stay in a variety of comfortable and clean hotels, hostels, lodges and cabins. Camping is an option on some courses and trip but not mandatory so you may not need to bring any bedding. Most places you will stay have saunas and/or hot tubs, which are a real luxury after a long day of paddling!</p>
                      </div>
                    </div>
                  </div>
                   <!--Panels-->
                <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#panel15" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> What is the weather like?</a>
                      </h4>
                    </div>
                    <div id="panel15" class="panel-collapse collapse in" >
                    <div class="panel-body">
                        <p>Paddle Patagonia’s trips travel through several different mountain terrains and at anytime the weather can change often and quickly. In general, the early season (Nov-Dec) may see cooler mornings/evenings with hot days (20+ C°). As we get closer to Jan-Feb, the weather stabilizes into warmer summer-like conditions (25+ C°). Rain and wind storms can happen at any time and the sun in Patagonia can be quite intense so a good sunscreen is advised.</p>
                      </div>
                    </div>
                  </div>
                   <!--Panels-->
                <div class="panel">
                    <div class="panel-heading">
                    <h4 class="panel-title">
                      <a href="#panel16" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> What is the Paddle Patagonia Payment and Cancellation Policy?</a>
                      </h4>
                    </div>
                    <div id="panel16" class="panel-collapse collapse in" >
                    <div class="panel-body">

                        <p><strong>Payment Schedule</strong>
                            A $500 deposit is required to confirm your trip. At least 50% of the balance is due 60 days prior to your trip departure date and the final balance is due 30 days before your trip departure date. However, whenever possible we appreciate and will accept the final 50% of the balance payment made upon arrival in Argentina in USD cash. Otherwise we ask for the full payment 30 days in advance by cheque, wire or money order. We unfortunately can no longer accept paypal (credit card) payments.</p>
                            <p>

                                By cheque or Money Order:
                                    <br>
                                    Issue to Paddle Patagonia Inc. and send by mail:
                                <br>
                                    73 Roseneath Cr.
                                <br>
                                    Kitchener, ON, N2E 1V8, Canada
                            </p>
                            <p><strong>Paddle Patagonia Cancellation Policy</strong>
                                If you need to cancel your reservation with Paddle Patagonia, please send your request by email to info@paddlepatagonia.com. If received 90 days before your trip’s departure date we will keep a $100 cancellation fee per person. If the request is received between 30 and 90 days before your trip the fee of $500 per person is non-refundable. If the request is received within 30 days of your trip there will be no refund but transfers or credits may be possible.</p>
                            <p>Instead of canceling a trip it may be possible to reschedule to a different date. If you decide to change your trip date the request is also required in writing by email to info@paddlepatagonia.com. All transfer requests are subject to trip and space availability and are not guaranteed. There will be a $100 fee charged to anyone changing their trip date including to the following year.</p>
                            <p>We also cannot issue refunds for late arrivals or early departures due to travel delays, flight cancellations or personal illness or injury. If you are concerned about having to cancel your trip we strongly suggest purchasing cancellation insurance to protect your investment.</p>
                            <p>There are no exceptions to these policies for any reason. We will endeavor to help you solve any travel issues that might occur during your trip but we cannot be held responsible for acts beyond our control such as natural disasters, flight schedule interruptions, strikes, theft, vandalism, government changes to rules and regulations and more.</p>
                            <p>Lastly, although we will endeavor to keep to the itinerary provided, all of Paddle Patagonia’s trips could be modified or delayed by bad weather, road conditions, river levels, trail conditions, transportation problems, sickness, or other factors beyond our control.</p>
                      </div>
                    </div>
                  </div>
                   <!--Panels-->


                </div>
              </div>
        </div>
    </div>
</section>





@endsection
