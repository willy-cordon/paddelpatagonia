<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');

// Events
Route::get('/events', 'PagesController@redirectUpcoming');
Route::get('/events/{slug}', 'PagesController@singleEvent')->name('singleEvent');
Route::get('/upcoming-events', 'PagesController@upcoming' )->name('upcoming');

// Trips
Route::get('/whitewater-mastering','PagesController@mastering');
//Route::get('/trips','PagesController@trips');
Route::get('/trips/{slug}','PagesController@singleTrip')->name('singleTrip');

// Others
Route::get('/adventure', 'PagesController@trips')->name('trips');
Route::get('/contact', 'PagesController@contact');
Route::get('/faqs', 'PagesController@faqs');
Route::get('/about-us', 'PagesController@about');


/**
 * Backend
 */

Route::group([
    'prefix' => config('admin-theme.path.panel'),
    'middleware' => 'auth',
], function () {

    Route::name('trips.')->prefix('trips')->group(function () {

        Route::get('/', 'TripController@index')->name('index');
        Route::get('create', 'TripController@create')->name('create');
        Route::post('create', 'TripController@store');
        Route::get('edit/{id}', 'TripController@edit')->name('edit');
        Route::post('edit/{id}', 'TripController@update');
        Route::delete('delete/{id}', 'TripController@delete')->name('delete');
    });
    Route::name('events.')->prefix('events')->group(function () {

        Route::get('/', 'EventController@index')->name('index');
        Route::get('create', 'EventController@create')->name('create');
        Route::post('create', 'EventController@store');
        Route::get('edit/{id}', 'EventController@edit')->name('edit');
        Route::post('edit/{id}', 'EventController@update');
        Route::delete('delete/{id}', 'EventController@delete')->name('delete');
    });
    Route::name('participants.')->prefix('participants')->group(function () {

        // Route::get('/', 'ParticipantController@index')->name('index');
        // Route::get('create', 'ParticipantController@create')->name('create');
        // Route::post('create', 'ParticipantController@store');
        // Route::get('edit/{id}', 'ParticipantController@edit')->name('edit');
        // Route::post('edit/{id}', 'ParticipantController@update');
        // Route::delete('delete/{id}', 'ParticipantController@delete')->name('delete');
        Route::get('/{eventId}', 'ParticipantController@eventParticipants')->name('byEvent');
    });
});
