<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Event extends Model
{
    use HasSlug;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'trip_id', 'slug', 'price', 'date', 'duration', 'availability', 'starting_point', 'summary'
    ];

    /**
     * Appends custom attributs
     *
     * @var array
     */
    protected $appends = ['trip_title', 'formated_date', 'formated_price'];

    /**
     * Cast model collumns
     *
     * @var array
     */
    protected $casts = ['date' => 'date:d, M'];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom(['trip_title', 'formated_date'])
            ->saveSlugsTo('slug');
    }

    /**
     * Trip relationship
     *
     * @return void
     */
    public function trip()
    {
        return $this->belongsTo(Trip::class);        
    }

    /**
     * Participants
     *
     * @return void
     */
    public function participants()
    {
        return $this->hasMany(Participant::class);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('withTrips', function (Builder $builder) {
            $builder->where('trip_id', '!=', null);
        });
    }

    /**
     * Query scope to get current events
     *
     * @param [type] $query
     * @return void
     */
    public function scopeCurrent($query)
    {
        return $query->where('date','>=', date('Y-m-d'));
    }

    /**
     * Custom attributes
     */
    
    public function getFormatedDateAttribute()
    {
        return date_format(date_create($this->date), 'd, M');
    }

    public function getTripTitleAttribute()
    {
        return $this->trip->title;
    }

    public function getFormatedPriceAttribute()
    {
        return '$ '. number_format($this->price, 2);
    }
}
