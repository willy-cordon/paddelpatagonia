<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;


class Trip extends Model implements HasMedia
{
    use HasSlug, HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'price', 'body', 'itinerary', 'accomodations', 'logistic'
    ];

    protected $appends = ['formated_price'];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    public function imageUrl()
    {
        if ($this->getFirstMediaUrl('gallery')) {
            return $this->getFirstMediaUrl('gallery', 'thumb');
        }
        return '/assets/images/mountain.png';        
    }

    public function getFormatedPriceAttribute()
    {
        return '$ '. number_format($this->price, 2);
    }

    /**
     * Create the gallery collection
     *
     * @return void
     */
    public function registerMediaCollections()
    {
        $this->addMediaCollection('gallery')
             ->registerMediaConversions(function (Media $media) {
                $this->addMediaConversion('fullhd')
                ->width(1920)
                ->height(1080);
        });

        $this->addMediaCollection('gallery')
            ->registerMediaConversions(function (Media $media) {
                $this->addMediaConversion('big')
                ->width(730)
                ->height(400);
            });

        $this->addMediaCollection('gallery')
            ->registerMediaConversions(function (Media $media) {
                $this->addMediaConversion('thumb')
                ->width(380)
                ->height(380);
            });
    }
}
