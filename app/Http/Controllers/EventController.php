<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use App\Http\Requests\EventRequest;

class EventController extends Controller
{
    public function index()
    {
        return view('admin-theme::event.index', [
            'events' => Event::paginate(10)
        ]);
    }

    public function create()
    {
       return view('admin-theme::event.create', [
            'trips' => \App\Trip::get(['id', 'title', 'price'])
           ]); 
    }

    public function store(EventRequest $request)
    {
        Event::create($request->all());
        
        request()->session()->flash('status', 'Event created succesfully!');
        return redirect(route('events.index')); 
    }
    
    public function edit($id)
    {
        return view('admin-theme::event.edit', [
            'event' => Event::findOrFail($id),
            'trips' => \App\Trip::get(['id', 'title', 'price'])
            ]);
    }
    
    public function update($id, EventRequest $request)
    {
        Event::findOrFail($id)->update($request->all());

        request()->session()->flash('status', 'Event was updated succesfully!');
        return redirect(route('events.index')); 
    }

    public function delete($id)
    {
        Event::destroy($id);

        request()->session()->flash('status', 'Event deleted!');
        return redirect(route('events.index')); 
    }    
}
