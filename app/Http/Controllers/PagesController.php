<?php

namespace App\Http\Controllers;

use App\Event;
use App\Trip;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home()
    {
        return view('home', [
            'events' => Event::current()->orderBy('date', 'asc')->take(4)->get()
            ]);
    }

    public function mastering(){
        return view('whitewater-mastering', [
            'events' => Event::current()->orderBy('date', 'asc')->take(4)->get()
            ]);
        }
        public function redirectUpcoming()
    {
        return redirect(route('upcoming'));
    }

    public function upcoming(){
        return view('upcoming-events', [
            'events' => Event::latest()->orderBy('date','asc')->paginate(6)
        ]);
      }

    public function singleEvent($slug)
    {
        return view('events', [
            'event' => Event::where('slug', $slug)->firstOrFail(),
            ]);

    }
    public function trips(){
        return view('trips', [
            'trips' => Trip::paginate(6)
        ]);

    }
    public function singleTrip($slug){
        return view('singleTrip', [
            'trip' => Trip::where('slug', $slug)->firstOrFail()
            ]);
        }

    public function contact(){
        return view('contact');
    }
    public function faqs(){
        return view('faqs');
    }
    public function about(){
        return view('about-us');
    }


}
