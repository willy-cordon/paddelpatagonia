<?php

namespace App\Http\Controllers;

use App\Participant;
use Illuminate\Http\Request;

class ParticipantController extends Controller
{
    public function eventParticipants($eventId)
    {
        return view('admin-theme::participant.index', [
            'participants' => Participant::where('event_id', $eventId)->get()
        ]);
    }
}
