<?php

namespace App\Http\Controllers;

use App\Trip;
use Illuminate\Http\Request;
use App\Http\Requests\TripRequest;

class TripController extends Controller
{
    public function index()
    {
        return view('admin-theme::trip.index', [
            'trips' => Trip::paginate(10)
        ]);
    }

    public function create()
    {
      
       return view('admin-theme::trip.create'); 
    }

    public function store(TripRequest $request)
    {
        $trip = Trip::create($request->all());
        $trip->addMedia($request->file('image'))->toMediaCollection('gallery');
        
        request()->session()->flash('status', 'Trip created succesfully!');
        return redirect(route('trips.index')); 
    }
    
    public function edit($id)
    {
        return view('admin-theme::trip.edit', ['trip' => Trip::findOrFail($id)]);
    }
    
    public function update($id, TripRequest $request)
    {
        $trip = Trip::findOrFail($id);
        $trip->update($request->all());
        if ($request->hasFile('image')) {
            $trip->clearMediaCollection('gallery');            
            $trip->addMedia($request->file('image'))->toMediaCollection('gallery');
        }

        request()->session()->flash('status', 'Trip was updated succesfully!');
        return redirect(route('trips.index')); 
    }

    public function delete($id)
    {
        Trip::destroy($id);

        request()->session()->flash('status', 'Trip deleted!');
        return redirect(route('trips.index')); 
    }
}
