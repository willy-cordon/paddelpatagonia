<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TripRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'price' => 'required|between:0,99999999,99',
            'body' => 'required',
            'itinerary' => 'required',
            'accomodations' => 'required',
            'logistic' => 'required',
        ];

        if ($this->has('id')) {
            $rules['image'] = 'mimes:png,jpg,jpeg';
        } else {
            $rules['image'] = 'required|mimes:png,jpg,jpeg';
        }

        return $rules;
    }
}
