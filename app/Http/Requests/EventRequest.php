<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trip_id' => 'required|exists:trips,id',
            'price' => 'required|between:0,99999999,99',
            'date' => 'required',
            'duration' => 'required|integer',
            'availability' => 'required|integer',
            'starting_point' => 'required',
            'summary' => 'required'
        ];
    }
}
